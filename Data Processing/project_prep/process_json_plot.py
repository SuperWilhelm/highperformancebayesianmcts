import json
import numpy as np
from matplotlib import pyplot as plt
from collections import defaultdict


with open('example.json') as json_file:
    data = json.load(json_file)

# print(data)
values = defaultdict(list)
iteration_keys = []
for datapoint in data.values():
    # print("fds")
    values[datapoint['planner']].append([(datapoint['data'][key]['loss_value']) for key in datapoint['data']])
    # print(datapoint)
    # print("\n")
for index in data["0"]['data']:
    iteration_keys.append(index)

# print(values)

# print(iteration_keys)

for value_label in values:
    values[value_label] = np.array(values[value_label])

sumarry_statistics = {}

for value_label in values:
    sumarry_statistics[value_label] = (np.mean(values[value_label], axis=0),
                                       np.std(values[value_label], axis=0))


# print(sumarry_statistics)
for key in sumarry_statistics:
    plt.plot(iteration_keys, sumarry_statistics[key][0], 'k-')
    plt.fill_between(iteration_keys,
                     sumarry_statistics[key][0] - sumarry_statistics[key][1],
                     sumarry_statistics[key][0] + sumarry_statistics[key][1])
plt.show()