import numpy as np
from scipy.stats import beta
import matplotlib.pyplot as plt
a, b = 20, 10
plt.rcParams["figure.figsize"] = (13, 4)

fig, ax = plt.subplots(1, 1)
x = np.linspace(0, 1, 1000)
ax.plot(x, beta.pdf(x, 1, 1), label='PDF after 0 wins, 0 losses')
ax.plot(x, beta.pdf(x, 21, 11), label='PDF after 20 wins, 10 losses')
ax.plot(x, beta.pdf(x, 201, 101), label='PDF after 200 wins, 100 losses')
ax.legend(loc='best', frameon=False)
plt.xlabel("Expected Utility")
plt.savefig('figure2.pdf', pad_inches='tight')

# plt.show()

