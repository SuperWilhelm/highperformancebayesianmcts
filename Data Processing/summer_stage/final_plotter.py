import matplotlib.pyplot as plt
import numpy as np
from matplotlib.lines import Line2D
from collections import defaultdict

import json

###########################################################################
# Helper Functions
###########################################################################


def average_std_its(lst):
    xs = [int(x[0]) for x in lst[0]]
    lst = np.array([[z for (x, y, z) in elem] for elem in lst])
    average = list(np.average(lst, axis=0))
    std = list(np.std(lst, axis=0))
    return (xs[0::2], average[0::2], std[0::2])


def average_std_time(lst):
    performance = np.array([[z for (x, y, z) in elem] for elem in lst])
    time_vals = np.array([[y for (x, y, z) in elem] for elem in lst])
    average_performance = list(np.average(performance, axis=0))
    average_time_vals = list(np.average(time_vals, axis=0))

    return (average_time_vals[0::2], average_performance[0::2], None)

###########################################################################
# matplotlib config
###########################################################################


plt.rcParams["figure.figsize"] = (20, 22)
font = {'size': 17}
plt.rc('font', **font)

cmap = {1: 'r', 2: 'g', 4: 'b', 8: 'c', 16: 'deepskyblue',
        32: 'slategray', 64: 'm', 128: 'navy'}

lmap = {'normal': 'o', 'bayesian_gaussian': '^', 'bayesian_numerical': 's'}

lmap_i = {0: 'o', 1: '^', 2: 's'}

# ###########################################################################
# # Leaf Plots
# ###########################################################################

data = {}

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/leaf_parallel/frequentest_results.json", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/leaf_parallel/bayesian_gaussian_results.json", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/leaf_parallel/bayesian_numerical_results_part_1.json", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/leaf_parallel/bayesian_numerical_results_part_2.json", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/leaf_parallel/bayesian_numerical_results_part_3.json", "r") as read_file:
    data.update(json.load(read_file))

processed_data = defaultdict(list)
for datapoint in data.values():
    processed_data[(datapoint["inference_mode"],
                    datapoint["parallel_pattern"],
                    datapoint["threads"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])


# Now average the data
it_processed_data = {}

for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(value)

time_processed_data = {}
for key, value in processed_data.items():
    time_processed_data[key] = average_std_time(value)


data_2 = {}
with open("../../BayesianMCTS/experiments/final_report_experiment_runs/leaf_parallel/without_multiplicity_experiment/slurm-363643.out", "r") as read_file:
    data_2.update(json.load(read_file))

# Now average the data
processed_data_2 = defaultdict(list)

for datapoint in data_2.values():
    processed_data_2[(datapoint["inference_mode"],
                      datapoint["parallel_pattern"],
                      datapoint["threads"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

it_processed_data_2 = {}
for key, value in processed_data_2.items():
    it_processed_data_2[key] = average_std_its(value)

time_processed_data_2 = {}
for key, value in processed_data_2.items():
    time_processed_data_2[key] = average_std_time(value)


# Leaf Parallel - Plot 1
fig, ax = plt.subplots(nrows=2, ncols=3, figsize=(22, 13))
labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)"]

for i, row in enumerate(ax[0]):
    row.set_xlabel("Iteration count\n"+labs.pop(0))
    row.set_ylabel("Average Decision Error")
    row.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
    row.set_ylim([0.001, 0.75])
    for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
        row.plot(it_processed_data[(i, 1, threads)][0], [1-x for x in it_processed_data[(i, 1, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)

    if i == 0:
        for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
            row.plot(it_processed_data_2[(i, 1, threads)][0], [1-x for x in it_processed_data_2[(i, 1, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)

    row.set_xscale('log')
    row.set_yscale('log')

for i, row in enumerate(ax[1]):
    row.set_xlabel("Time (Seconds)\n"+labs.pop(0))
    row.set_ylabel("Average Decision Error")
    row.set_ylim([0.001, 0.75])
    for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
        row.plot(time_processed_data[(i, 1, threads)][0], [1-x for x in time_processed_data[(i, 1, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
    if i == 0:
        for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
            row.plot(time_processed_data_2[(i, 1, threads)][0], [1-x for x in time_processed_data_2[(i, 1, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)

    row.set_xscale('log')
    row.set_yscale('log')

lines = []
labels = []

Line, Label = fig.axes[4].get_legend_handles_labels()
lines.extend(Line)
labels.extend(Label)

# lines.append(Line2D([0], [0], color='black', linestyle='dotted'))
# labels.append("Without Multiplicity")
fig.legend(lines, labels, loc='lower center', bbox_to_anchor=(0.5, 0.00), markerscale=0, ncol=9)
plt.margins(0.15, tight=True)
plt.savefig('final_plots/leaf_1.pdf')
plt.clf()

#Plot 2:
plt.rcParams["figure.figsize"] = (18, 27)
labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)"]

fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(29, 13))
count = 1
for row in ax:
    for col in row:
        col.set_xlabel("Iteration count\n"+labs.pop(0))
        col.set_ylabel("Average Decision Error")
        col.set_xscale('log')
        col.set_yscale('log')
        col.set_ylim([0.001, 0.75])
        for i, thread in enumerate(['normal', 'bayesian_gaussian', 'bayesian_numerical']):
            col.plot(it_processed_data[(i, 1, count)][0], [1-x for x in it_processed_data[(i, 1, count)][1]], markersize=6, marker=lmap[thread],
                     color=cmap[count], markevery=0.1)

        col.plot(it_processed_data_2[(0, 1, count)][0], [1-x for x in it_processed_data_2[(0, 1, count)][1]], linestyle="dotted", markersize=6, marker=lmap['normal'],
                 color=cmap[count], markevery=0.1)
        count *= 2

lines = [Line2D([0], [0], color='k', marker='o', linestyle=''),
         Line2D([0], [0], color='k', marker='^', linestyle=''),
         Line2D([0], [0], color='k', marker='s', linestyle='')]

labels = ["UCT",
          "Bayesian Gaussian",
          "Bayesian Numerical"]

fig.legend(lines, labels,
           loc='lower center', bbox_to_anchor=(0.5, 0), ncol=3)
plt.savefig('final_plots/leaf_2.pdf')
plt.clf()

plt.rcParams["figure.figsize"] = (18, 27)
labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)"]

fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(29, 13))
count = 1
for row in ax:
    for col in row:
        col.set_xlabel("Time (Seconds)\n"+labs.pop(0))
        col.set_ylabel("Average Decision Error")
        col.set_xscale('log')
        col.set_yscale('log')
        col.set_ylim([0.001, 0.75])
        for i, thread in enumerate(['normal', 'bayesian_gaussian', 'bayesian_numerical']):
            col.plot(time_processed_data[(i, 1, count)][0], [1-x for x in time_processed_data[(i, 1, count)][1]], markersize=6, linestyle="solid", marker=lmap[thread],
                     color=cmap[count], markevery=0.1)
        col.plot(time_processed_data_2[(0, 1, count)][0], [1-x for x in time_processed_data_2[(0, 1, count)][1]], linestyle="dotted", markersize=6, marker=lmap['normal'],
                 color=cmap[count], markevery=0.1)

        count *= 2

lines = [Line2D([0], [0], color='k', marker='o', linestyle=''),
         Line2D([0], [0], color='k', marker='^', linestyle=''),
         Line2D([0], [0], color='k', marker='s', linestyle='')]

labels = ["UCT",
          "Bayesian Gaussian",
          "Bayesian Numerical"]


fig.legend(lines, labels,
           loc='lower center', bbox_to_anchor=(0.5, 0), ncol=3)
plt.savefig('final_plots/leaf_3.pdf')
plt.clf()
###########################################################################
# Root Plots
###########################################################################

data = {}

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363474.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363475.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363476.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363477.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363478.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363479.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363480.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/root_parallel/slurm-363482.out", "r") as read_file:
    data.update(json.load(read_file))


processed_data = defaultdict(list)
for datapoint in data.values():
    processed_data[(datapoint["inference_mode"],
                    datapoint["parallel_pattern"],
                    datapoint["size"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])


# Now average the data
it_processed_data = {}

for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(value)

time_processed_data = {}
for key, value in processed_data.items():
    time_processed_data[key] = average_std_time(value)

#Plot 1
plt.rcParams["figure.figsize"] = (20, 22)
fig, ax = plt.subplots(nrows=2, ncols=3, figsize=(22, 13))
labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)"]

for i, row in enumerate(ax[0]):
    row.set_xlabel("Iteration count\n"+labs.pop(0))
    row.set_ylabel("Average Decision Error")
    row.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
    row.set_ylim([0.001, 0.75])
    for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
        row.plot(it_processed_data[(i, 6, threads)][0], [1-x for x in it_processed_data[(i, 6, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
        row.plot(it_processed_data[(i, 7, threads)][0], [1-x for x in it_processed_data[(i, 7, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)
    row.set_xscale('log')
    row.set_yscale('log')

for i, row in enumerate(ax[1]):
    row.set_xlabel("Time (Seconds)\n"+labs.pop(0))
    row.set_ylabel("Average Decision Error")
    row.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
    row.set_ylim([0.001, 0.75])
    for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
        row.plot(time_processed_data[(i, 6, threads)][0], [1-x for x in time_processed_data[(i, 6, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
        row.plot(time_processed_data[(i, 7, threads)][0], [1-x for x in time_processed_data[(i, 7, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)
    row.set_xscale('log')
    row.set_yscale('log')

lines = []
labels = []

for ax in fig.axes:
    Line, Label = ax.get_legend_handles_labels()
    lines.extend(Line)
    labels.extend(Label)
    break

lines = lines[0::2]
labels = labels[0::2]

# lines.append(Line2D([0], [0], color='black', linestyle='dotted'))
# labels.append("Weight sharing")

fig.legend(lines, labels, loc='lower center', bbox_to_anchor=(0.5, 0.00), markerscale=0, ncol=8)
plt.margins(0.15, tight=True)
plt.savefig('final_plots/root_1.pdf')
plt.clf()
#Plot 2:
plt.rcParams["figure.figsize"] = (20, 27)


labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)"]

fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(29, 13))
threads = 1
for row in ax:
    for col in row:
        col.set_xlabel("Iteration count\n"+labs.pop(0))
        col.set_ylabel("Average Decision Error")
        col.set_xscale('log')
        col.set_yscale('log')
        # col.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
        col.set_ylim([0.001, 0.75])
        for i, thread in enumerate(['normal', 'bayesian_gaussian', 'bayesian_numerical']):
            col.plot(it_processed_data[(i, 6, threads)][0], [1-x for x in it_processed_data[(i, 6, threads)][1]], marker=lmap_i[i], color=cmap[threads], markersize=6, label=threads, markevery=0.1)
            col.plot(it_processed_data[(i, 7, threads)][0], [1-x for x in it_processed_data[(i, 7, threads)][1]], marker=lmap_i[i], linestyle="dotted", color=cmap[threads], markersize=6, label=threads, markevery=0.1)

        threads *= 2


lines = [Line2D([0], [0], color='k', marker='o', linestyle=''),
         Line2D([0], [0], color='k', marker='^', linestyle=''),
         Line2D([0], [0], color='k', marker='s', linestyle='')]

labels = ["UCT",
          "Bayesian Gaussian",
          "Bayesian Numerical"]

fig.legend(lines, labels,
           loc='lower center', bbox_to_anchor=(0.5, 0.0),
           ncol=3)
plt.savefig('final_plots/root_2.pdf')
plt.clf()
#Plot 3:

plt.rcParams["figure.figsize"] = (20, 27)

labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)"]

fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(29, 13))
threads = 1
for row in ax:
    for col in row:
        col.set_xlabel("Time (Seconds) \n"+labs.pop(0))
        col.set_ylabel("Average Decision Error")
        col.set_xscale('log')
        col.set_yscale('log')
        # col.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
        col.set_ylim([0.001, 0.75])
        for i, thread in enumerate(['normal', 'bayesian_gaussian', 'bayesian_numerical']):
            col.plot(time_processed_data[(i, 6, threads)][0], [1-x for x in time_processed_data[(i, 6, threads)][1]], marker=lmap_i[i], color=cmap[threads], markersize=6, label=threads, markevery=0.1)
            col.plot(time_processed_data[(i, 7, threads)][0], [1-x for x in time_processed_data[(i, 7, threads)][1]], marker=lmap_i[i], linestyle="dotted", color=cmap[threads], markersize=6, label=threads, markevery=0.1)

        threads *= 2


lines = [Line2D([0], [0], color='k', marker='o', linestyle=''),
         Line2D([0], [0], color='k', marker='^', linestyle=''),
         Line2D([0], [0], color='k', marker='s', linestyle='')]

labels = ["UCT",
          "Bayesian Gaussian",
          "Bayesian Numerical"]

fig.legend(lines, labels,
           loc='lower center', bbox_to_anchor=(0.5, 0.00),
           ncol=3)
plt.savefig('final_plots/root_3.pdf')
plt.clf()
############################################################################################
###################################     Tree Plots      ####################################
############################################################################################

data = {}

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-363547.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-363548.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-363551.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-363552.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-363553.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-363554.out", "r") as read_file:
    data.update(json.load(read_file))


with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-369855.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-369856.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-369857.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-369858.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-370084.out", "r") as read_file:
    data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/slurm-370085.out", "r") as read_file:
    data.update(json.load(read_file))

processed_data = defaultdict(list)
for datapoint in data.values():
    processed_data[(datapoint["inference_mode"],
                    datapoint["parallel_pattern"],
                    datapoint["threads"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])


# for key, value in processed_data.items():
#   print("{0}: {1}".format(key, len(value)))
# Now average the data
it_processed_data = {}

for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(value)

time_processed_data = {}
for key, value in processed_data.items():
    time_processed_data[key] = average_std_time(value)

#Plot 1
plt.rcParams["figure.figsize"] = (20, 22)
fig, ax = plt.subplots(nrows=2, ncols=3, figsize=(22, 13))
labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)"]

for i, row in enumerate(ax[0]):
    row.set_xlabel("Iteration count\n"+labs.pop(0))
    row.set_ylabel("Average Decision Error")
    row.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
    row.set_ylim([0.001, 0.75])

    for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
        if threads >= 32 and i==2:
            continue
        row.plot([x/threads for x in it_processed_data[(i, 3, threads)][0]], [1-x for x in it_processed_data[(i, 3, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
        row.plot([x/threads for x in it_processed_data[(i, 2, threads)][0]], [1-x for x in it_processed_data[(i, 2, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)
    row.set_xscale('log')
    row.set_yscale('log')

for i, row in enumerate(ax[1]):
    row.set_xlabel("Time (Seconds)\n"+labs.pop(0))
    row.set_ylabel("Average Decision Error")
    row.set_xticks([0, 2000, 4000,  6000, 8000, 10000])
    row.set_ylim([0.001, 0.75])
    for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
        if threads >= 32 and i==2:
            continue
        row.plot(time_processed_data[(i, 3, threads)][0], [1-x for x in time_processed_data[(i, 3, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
        row.plot(time_processed_data[(i, 2, threads)][0], [1-x for x in time_processed_data[(i, 2, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)
    row.set_xscale('log')
    row.set_yscale('log')

lines = []
labels = []
for ax in fig.axes:
    Line, Label = ax.get_legend_handles_labels()
    lines.extend(Line)
    labels.extend(Label)
    break

lines = lines[0::2]
labels = labels[0::2]

# lines.append(Line2D([0], [0], color='black', linestyle='dotted'))
# labels.append("Virtual Loss")


fig.legend(lines, labels, loc='lower center', bbox_to_anchor=(0.5, 0.00), markerscale=0, ncol=8)
plt.margins(0.15, tight=True)
plt.savefig('final_plots/tree_1.pdf')
plt.clf()
#Plot 2:

plt.rcParams["figure.figsize"] = (20, 27)

labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)"]

fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(29, 13))
count = 1
for row in ax:
    for col in row:
        col.set_xlabel("Iteration count\n"+labs.pop(0))
        col.set_ylabel("Average Decision Error")
        col.set_xscale('log')
        col.set_yscale('log')
        col.set_ylim([0.001, 0.75])
        for i, thread in enumerate(['normal', 'bayesian_gaussian', 'bayesian_numerical']):
            if count >= 32 and i == 2:
                continue
            col.plot([x/count for x in it_processed_data[(i, 3, count)][0]], [1-x for x in it_processed_data[(i, 3, count)][1]], marker=lmap_i[i], color=cmap[count], markersize=6, label=count, markevery=0.1)
            col.plot([x/count for x in it_processed_data[(i, 2, count)][0]], [1-x for x in it_processed_data[(i, 2, count)][1]], marker=lmap_i[i], linestyle="dotted", color=cmap[count], markersize=6, label=count, markevery=0.1)

        count *= 2

lines = [Line2D([0], [0], color='k', marker='o', linestyle=''),
         Line2D([0], [0], color='k', marker='^', linestyle=''),
         Line2D([0], [0], color='k', marker='s', linestyle='')]

labels = ["UCT",
          "Bayesian Gaussian",
          "Bayesian Numerical"]

fig.legend(lines, labels,
           loc='lower center', bbox_to_anchor=(0.5, 0.00),
           ncol=3)
plt.savefig('final_plots/tree_2.pdf')
plt.clf()
# Plot 3:

plt.rcParams["figure.figsize"] = (20, 27)

labs = ["(a)", "(b)", "(c)", "(d)", "(e)", "(f)", "(g)", "(h)"]

fig, ax = plt.subplots(nrows=2, ncols=4, figsize=(29, 13))
count = 1
for row in ax:
    for col in row:
        col.set_xlabel("Time (Seconds) \n"+labs.pop(0))
        col.set_ylabel("Average Decision Error")
        col.set_xscale('log')
        col.set_yscale('log')
        col.set_ylim([0.001, 0.75])
        for i, thread in enumerate(['normal', 'bayesian_gaussian', 'bayesian_numerical']):
            if count >= 32 and i == 2:
                continue
            col.plot(time_processed_data[(i, 3, count)][0], [1-x for x in time_processed_data[(i, 3, count)][1]], marker=lmap_i[i], color=cmap[count], markersize=6, label=count, markevery=0.1)
            col.plot(time_processed_data[(i, 2, count)][0], [1-x for x in time_processed_data[(i, 2, count)][1]], marker=lmap_i[i], linestyle="dotted", color=cmap[count], markersize=6, label=count, markevery=0.1)
        count *= 2

lines = [Line2D([0], [0], color='k', marker='o', linestyle=''),
         Line2D([0], [0], color='k', marker='^', linestyle=''),
         Line2D([0], [0], color='k', marker='s', linestyle='')]

labels = ["UCT",
          "Bayesian Gaussian",
          "Bayesian Numerical"]

fig.legend(lines, labels,
           loc='lower center', bbox_to_anchor=(0.5, 0.00),
           ncol=3)
plt.savefig('final_plots/tree_3.pdf')
plt.clf()

###########################################################################
# Lock Location
###########################################################################

fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(20, 8))
labs = ["(a)", "(b)"]

new_data = {}

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/run_lock_experiments/slurm-369873.out", "r") as read_file:
    new_data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/run_lock_experiments/slurm-369874.out", "r") as read_file:
    new_data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/run_lock_experiments/slurm-369875.out", "r") as read_file:
    new_data.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/tree_parallel/run_lock_experiments/slurm-369880.out", "r") as read_file:
    new_data.update(json.load(read_file))

for datapoint in new_data.values():
    processed_data[(datapoint["inference_mode"],
                    datapoint["parallel_pattern"],
                    datapoint["threads"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# for key, value in processed_data.items():
#   print("{0}: {1}".format(key, len(value)))
# Now average the data
it_processed_data = {}

for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(value)

time_processed_data = {}
for key, value in processed_data.items():
    time_processed_data[key] = average_std_time(value)

ax[0].set_xlabel("Iteration count\n"+labs.pop(0))
ax[0].set_ylabel("Average Decision Error")
ax[0].set_xticks([0, 2000, 4000,  6000, 8000, 10000])
ax[0].set_ylim([0.001, 0.75])

for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
    ax[0].plot([x/threads for x in it_processed_data[(1, 3, threads)][0]], [1-x for x in it_processed_data[(1, 3, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
    ax[0].plot([x/threads for x in it_processed_data[(1, 5, threads)][0]], [1-x for x in it_processed_data[(1, 5, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)
ax[0].set_xscale('log')
ax[0].set_yscale('log')

ax[1].set_xlabel("Time (Seconds)\n"+labs.pop(0))
ax[1].set_ylabel("Average Decision Error")
ax[1].set_xticks([0, 2000, 4000,  6000, 8000, 10000])
ax[1].set_ylim([0.001, 0.75])
for threads in [1, 2, 4, 8, 16, 32, 64, 128]:
    ax[1].plot(time_processed_data[(1, 3, threads)][0], [1-x for x in time_processed_data[(1, 3, threads)][1]], linestyle="solid", color=cmap[threads], markersize=6, label=threads)
    ax[1].plot(time_processed_data[(1, 5, threads)][0], [1-x for x in time_processed_data[(1, 5, threads)][1]], linestyle="dotted", color=cmap[threads], markersize=6, label=threads)
ax[1].set_xscale('log')
ax[1].set_yscale('log')

lines = []
labels = []
for ax in fig.axes:
    Line, Label = ax.get_legend_handles_labels()
    lines.extend(Line)
    labels.extend(Label)
    break

lines = lines[0::2]
labels = labels[0::2]

lines.append(Line2D([0], [0], color='black', linestyle='dotted'))
labels.append("Virtual Loss")


fig.legend(lines, labels, loc='center right',)
# plt.margins(0.15, tight=True)
plt.savefig('final_plots/tree_lock_location.pdf')
plt.clf()
###########################################################################
# Speedup Plots
###########################################################################

plt.figure(figsize=(20, 8))


data_999 = {}

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-369970.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-369971.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370057.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370058.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370059.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370060.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370061.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370062.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370063.out", "r") as read_file:
    data_999.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/slurm-370064.out", "r") as read_file:
    data_999.update(json.load(read_file))

data_99 = {}

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370797.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370798.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370800.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370801.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370802.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370803.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370804.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370805.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370806.out", "r") as read_file:
    data_99.update(json.load(read_file))

with open("../../BayesianMCTS/experiments/final_report_experiment_runs/bayesian_speedup/99_percent_optimality/slurm-370808.out", "r") as read_file:
    data_99.update(json.load(read_file))

processed_data_999 = defaultdict(list)

for key, value in data_999.items():
    try:
        processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append(value['data'][0])
    except Exception as e:
        if value["size"] == 32:
            processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 120+49.185)[0])
        elif value["size"] == 64:
            processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+10.325)[0])
        elif value["size"] == 128:
            processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+15.239)[0])
        pass


processed_data_99 = defaultdict(list)

for key, value in data_99.items():
    try:
        processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append(value['data'][0])
    except Exception as e:
        print("except")
        if value["size"] == 32:
            processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 120+49.185)[0])
        elif value["size"] == 64:
            processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+10.325)[0])
        elif value["size"] == 128:
            processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+15.239)[0])
        pass

processed_data_1_99 = {}
for key, value in processed_data_99.items():
    processed_data_1_99[key] = np.mean(value)

tree_99 = [val/key[2] for key, val in processed_data_1_99.items() if key[0] == 3]
tree_99 = [tree_99[0]/x for x in tree_99]
leaf_99 = [val for key, val in processed_data_1_99.items() if key[0] == 1]
leaf_99 = [leaf_99[0]/x for x in leaf_99]
root_99 = [val for key, val in processed_data_1_99.items() if key[0] == 7]
root_99 = [root_99[0]/x for x in root_99]

plt.plot([1, 2, 4, 8, 16, 32, 64, 128], tree_99, linestyle='dotted', color='r', label='tree-99%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], leaf_99, linestyle='dotted', color='g', label='leaf-99%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], root_99, linestyle='dotted', color='b', label='root-99%')

processed_data_1_999 = {}
for key, value in processed_data_999.items():
    processed_data_1_999[key] = np.mean(value)

tree_999 = [val/key[2] for key, val in processed_data_1_999.items() if key[0] == 3]
tree_999 = [tree_999[0]/x for x in tree_999]
leaf_999 = [val for key, val in processed_data_1_999.items() if key[0] == 1]
leaf_999 = [leaf_999[0]/x for x in leaf_999]
root_999 = [val for key, val in processed_data_1_999.items() if key[0] == 7]
root_999 = [root_999[0]/x for x in root_999]

plt.plot([1, 2, 4, 8, 16, 32, 64, 128], tree_999, color='r', label='tree-99.9%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], leaf_999, color='g', label='leaf-99.9%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], root_999, color='b', label='root-99.9%')
plt.xlabel("Processing Element count (PEs)")
plt.ylabel("Speedup (simulations/PEs)")
# plt.legend(loc="upper left")
lines = [Line2D([0], [0], color='k', linestyle='dotted'),
         Line2D([0], [0], color='k', linestyle='solid'),
         Line2D([0], [0], color='r', linestyle='solid'),
         Line2D([0], [0], color='g', linestyle='solid'),
         Line2D([0], [0], color='b', linestyle='solid')]

labels = ["99% optimality",
          "99.9% optimality",
          "Tree Stratagy",
          "Leaf Stratagy",
          "Root Stratagy"]
axis = plt.gca()
axis.legend(lines, labels)

plt.savefig('final_plots/bayesian_speedup_its.pdf')
plt.clf()

processed_data_999 = defaultdict(list)

for key, value in data_999.items():
    try:
        processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append(value['data'][1])
    except Exception as e:
        if value["size"] == 32:
            processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 120+49.185)[1])
        elif value["size"] == 64:
            processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+10.325)[1])
        elif value["size"] == 128:
            processed_data_999[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+15.239)[1])
        pass

processed_data_99 = defaultdict(list)

for key, value in data_99.items():
    try:
        processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append(value['data'][1])
    except Exception as e:
        if value["size"] == 32:
            processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 120+49.185)[1])
        elif value["size"] == 64:
            processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+10.325)[1])
        elif value["size"] == 128:
            processed_data_99[(value["parallel_pattern"], value["size"], value["threads"])].append((1000000, 180+15.239)[1])
        pass

processed_data_1_99 = {}
for key, value in processed_data_99.items():
    processed_data_1_99[key] = np.mean(value)

tree_99 = [val for key, val in processed_data_1_99.items() if key[0] == 3]
tree_99 = [tree_99[0]/x for x in tree_99]
leaf_99 = [val for key, val in processed_data_1_99.items() if key[0] == 1]
leaf_99 = [leaf_99[0]/x for x in leaf_99]
root_99 = [val for key, val in processed_data_1_99.items() if key[0] == 7]
root_99 = [root_99[0]/x for x in root_99]

plt.plot([1, 2, 4, 8, 16, 32, 64, 128], tree_99, linestyle='dotted', color='r', label='tree-99%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], leaf_99, linestyle='dotted', color='g',label='leaf-99%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], root_99, linestyle='dotted', color='b',label='root-99%')
processed_data_1_999 = {}
for key, value in processed_data_999.items():
    processed_data_1_999[key] = np.mean(value)

tree_999 = [val for key, val in processed_data_1_999.items() if key[0] == 3]
tree_999 = [tree_999[0]/x for x in tree_999]
leaf_999 = [val for key, val in processed_data_1_999.items() if key[0] == 1]
leaf_999 = [leaf_999[0]/x for x in leaf_999]
root_999 = [val for key, val in processed_data_1_999.items() if key[0] == 7]
root_999 = [root_999[0]/x for x in root_999]

plt.plot([1, 2, 4, 8, 16, 32, 64, 128], tree_999, color='r', label='tree-99.9%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], leaf_999, color='g', label='leaf-99.9%')
plt.plot([1, 2, 4, 8, 16, 32, 64, 128], root_999, color='b', label='root-99.9%')

plt.xlabel("Processing Element count (PEs)")
plt.ylabel("Speedup (time)")
# plt.legend(loc="upper left")
lines = [Line2D([0], [0], color='k', linestyle='dotted'),
         Line2D([0], [0], color='k', linestyle='solid'),
         Line2D([0], [0], color='r', linestyle='solid'),
         Line2D([0], [0], color='g', linestyle='solid'),
         Line2D([0], [0], color='b', linestyle='solid')]

labels = ["99% optimality",
          "99.9% optimality",
          "Tree Stratagy",
          "Leaf Stratagy",
          "Root Stratagy"]
axis = plt.gca()
axis.legend(lines, labels)

plt.savefig('final_plots/bayesian_speedup_time.pdf')
plt.clf()


# Then this section is finished! :DD []
