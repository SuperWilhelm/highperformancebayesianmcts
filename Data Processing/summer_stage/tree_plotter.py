import json
import numpy as np
from matplotlib import pyplot as plt
from collections import defaultdict
import scipy.interpolate


plt.rcParams.update({'font.size': 22})

def average_std_its(lst):
    xs = [x[0] for x in lst[0]]
    lst = np.array([[z for (x, y, z) in elem] for elem in lst])

    average = list(np.average(lst, axis=0))
    std = list(np.std(lst, axis=0))
    return (xs, average, std)


def average_std_time(lst):
    max_time = max([max([q for (p, q, r) in x]) for x in lst])
    lst = [scipy.interpolate.interp1d([0]+[q for (p, q, r) in x],
                                      [0.5]+[r for (p, q, r) in x],
                                      bounds_error=False,
                                      fill_value=[r for (p, q, r) in x][-1]) for x in lst]
#     lst = np.array([[(0, 0.5)] + [(y, z) for (x, y, z) in elem] for elem in lst])
# y_interp = scipy.interpolate.interp1d(x, y)
    # max_val = max([x[-1][0] for x in lst])
    xs = np.linspace(0, max_time, num=200)
    ys = [np.average([l(x) for l in lst]) for x in xs]
    std = [np.std([l(x) for l in lst]) for x in xs]
    return xs, ys, std


# with open('../../BayesianMCTS/experiments/tree_experiments_v1/tree_exp_2.json') as json_file:
#     data = json.load(json_file)

# # First plot vs iterations

# processed_data = defaultdict(list)


# for datapoint in data.values():
#     # print("")
#     # print(datapoint["data"])
#     # [(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"]]
#     processed_data[(datapoint["parallel_pattern"], datapoint["threads"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# # Now average the data
# it_processed_data = {}

# for key, value in processed_data.items():
#     it_processed_data[key] = average_std_its(processed_data[key])

# time_processed_data = {}
# for key, value in processed_data.items():
#     time_processed_data[key] = average_std_time(processed_data[key])


# for key, value in it_processed_data.items():
#     if key[0] == 3:
#         plt.plot([0]+value[0], [0.5]+value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Local Locking with No virtual loss")
# ax = plt.gca()
# ax.set_xticks(ax.get_xticks()[::10])
# # plt.xscale('log', basex=2) 
# plt.show()

# for key, value in it_processed_data.items():
#     if key[0] == 2:
#         plt.plot([0]+value[0], [0.5]+value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Local Locking with virtual loss")
# ax = plt.gca()
# ax.set_xticks(ax.get_xticks()[::10])
# plt.show()

# for key, value in it_processed_data.items():
#     if key[0] == 5:
#         plt.plot([0]+value[0], [0.5]+value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Global Locking with No virtual loss")
# ax = plt.gca()
# ax.set_xticks(ax.get_xticks()[::10])
# plt.show()

# for key, value in it_processed_data.items():
#     if key[0] == 4:
#         plt.plot([0]+value[0], [0.5]+value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Global Locking with virtual loss")
# ax = plt.gca()
# ax.set_xticks(ax.get_xticks()[::10])
# plt.show()


# # Now some in
# def keymap(i):
#     if i == 2:
#         return "Tree-Local-VL"
#     if i == 3:
#         return "Tree-Local-no_VL"
#     if i == 4:
#         return "Tree-Global-VL"
#     if i == 5:
#         return "Tree-Global-no_VL"


# for key, value in it_processed_data.items():
#     if key[1] == 36:
#         plt.plot([0]+value[0], [0.5]+value[1], label=keymap(key[0]))
# plt.legend(loc="lower right")
# plt.title("36 threads")
# ax = plt.gca()
# ax.set_xticks(ax.get_xticks()[::10])
# plt.show()


# times = {}


# # now plot performance vs time

# for key, value in time_processed_data.items():
#     if key[0] == 3:
#         plt.plot(value[0], value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Local Locking with No virtual loss")
# plt.show()

# for key, value in time_processed_data.items():
#     if key[0] == 2:
#         plt.plot(value[0], value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Local Locking with virtual loss")
# plt.show()

# for key, value in time_processed_data.items():
#     if key[0] == 5:
#         plt.plot(value[0], value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Global Locking with No virtual loss")
# plt.show()

# for key, value in time_processed_data.items():
#     if key[0] == 4:
#         plt.plot(value[0], value[1], label=key[1])
# plt.legend(loc="lower right")
# plt.title("Global Locking with virtual loss")
# plt.show()

# # Inter method 36 threads
# for key, value in time_processed_data.items():
#     if key[1] == 36:
#         plt.plot(value[0], value[1], label=keymap(key[0]))
# plt.legend(loc="lower right")
# plt.title("36 threads")
# plt.show()

# for key, value in time_processed_data.items():
#     times[(key[0], key[1])] = value[0][-1]

# time_2 = [y for (x, y) in times.items() if x[0] == 2]
# time_3 = [y for (x, y) in times.items() if x[0] == 3]
# time_4 = [y for (x, y) in times.items() if x[0] == 4]
# time_5 = [y for (x, y) in times.items() if x[0] == 5]

# time_2 = [time_2[0]/x for x in time_2]
# time_3 = [time_3[0]/x for x in time_3]
# time_4 = [time_4[0]/x for x in time_4]
# time_5 = [time_5[0]/x for x in time_5]

# # print(time_2)
# # print(time_3)
# # print(time_4)
# # print(time_5)

# plt.plot([1, 2, 4, 9, 18, 36], time_2, label=keymap(2))
# plt.plot([1, 2, 4, 9, 18, 36], time_3, label=keymap(3))
# plt.plot([1, 2, 4, 9, 18, 36], time_4, label=keymap(4))
# plt.plot([1, 2, 4, 9, 18, 36], time_5, label=keymap(5))
# plt.legend(loc="lower right")
# plt.title("Speedup")
# plt.show()

# # Plot some root para
# with open('../../BayesianMCTS/experiments/root_experiments_v1/run_1_procs_1.json') as json_file:
#     data1 = json.load(json_file)

# with open('../../BayesianMCTS/experiments/root_experiments_v1/run_1_procs_2.json') as json_file:
#     data2 = json.load(json_file)

# with open('../../BayesianMCTS/experiments/root_experiments_v1/run_1_procs_4.json') as json_file:
#     data4 = json.load(json_file)

# with open('../../BayesianMCTS/experiments/root_experiments_v1/run_1_procs_9.json') as json_file:
#     data9 = json.load(json_file)

# with open('../../BayesianMCTS/experiments/root_experiments_v1/run_1_procs_18.json') as json_file:
#     data18 = json.load(json_file)

# with open('../../BayesianMCTS/experiments/root_experiments_v1/run_1_procs_36.json') as json_file:
#     data36 = json.load(json_file)


# processed_data = defaultdict(list)

# for datapoint in data1.values():
#     processed_data[datapoint["size"]].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# for datapoint in data2.values():
#     processed_data[datapoint["size"]].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# for datapoint in data4.values():
#     processed_data[datapoint["size"]].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# for datapoint in data9.values():
#     processed_data[datapoint["size"]].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# for datapoint in data18.values():
#     processed_data[datapoint["size"]].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# for datapoint in data36.values():
#     processed_data[datapoint["size"]].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# # Now average the data
# it_processed_data = {}

# time_processed_data = {}


# for key, value in processed_data.items():
#     it_processed_data[key] = average_std_its(processed_data[key])

# time_processed_data = {}
# for key, value in processed_data.items():
#     time_processed_data[key] = average_std_time(processed_data[key])


# for key, value in it_processed_data.items():
#     plt.plot([0] + value[0], [0.5]+value[1], label=key)
# plt.legend(loc="lower right")
# ax = plt.gca()
# ax.set_xticks(ax.get_xticks()[::10])
# plt.title("Root parallel by iterations")
# plt.show()


# for key, value in time_processed_data.items():
#     plt.plot(value[0], value[1], label=key)
# plt.legend(loc="lower right")
# plt.title("Root parallel by time")
# plt.show()




# Plot some root para
with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312904.out') as json_file:
    data1 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312906.out') as json_file:
    data2 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312907.out') as json_file:
    data4 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312908.out') as json_file:
    data8 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312909.out') as json_file:
    data16 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312910.out') as json_file:
    data32 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312911.out') as json_file:
    data64 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/slurm-312912.out') as json_file:
    data128 = json.load(json_file)


processed_data = defaultdict(list)

it_processed_data = {}

time_processed_data = {}

for key, datapoint in data1.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data2.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data4.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data8.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data16.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data32.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data64.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data128.items():
    processed_data[(datapoint["size"], int(key) < 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])


for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(processed_data[key])

def col_map(n):
    if n == 1:
        return 'b'
    if n == 2:
        return 'g'
    if n == 4:
        return 'r'
    if n == 8:
        return 'c'
    if n == 16:
        return 'm'
    if n == 32:
        return 'k'
    if n == 64:
        return '0.22'
    if n == 128:
        return '0.6'


for key, value in it_processed_data.items():
    if key[1]:
        plt.plot([0] + value[0], [0.5]+value[1], "--+", color=col_map(key[0]),  label=key[0])
    else:
        plt.plot([0] + value[0], [0.5] + value[1], color=col_map(key[0]), label=key[0])
plt.legend(loc="lower right")
plt.xscale('log',basex=2) 
ax = plt.gca()
ax.set_xticks(ax.get_xticks()[::10])
plt.title("Root parallel by iterations")
plt.show()



# Plot some root para
with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316058.out') as json_file:
    data1 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316058.out') as json_file:
    data2 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316057.out') as json_file:
    data4 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316056.out') as json_file:
    data8 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316055.out') as json_file:
    data16 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316054.out') as json_file:
    data32 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316053.out') as json_file:
    data64 = json.load(json_file)

with open('../../BayesianMCTS/experiments/root_parallel_experiments/fix_randomness/slurm-316052.out') as json_file:
    data128 = json.load(json_file)


processed_data = defaultdict(list)

it_processed_data = {}

time_processed_data = {}

for key, datapoint in data1.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data2.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data4.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data8.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data16.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data32.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data64.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

for key, datapoint in data128.items():
    processed_data[(datapoint["size"], int(key) > 2000000)].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])


for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(processed_data[key])

for key, value in it_processed_data.items():
    if key[1]:
        plt.plot([0] + value[0], [0.5]+value[1], "--+", color=col_map(key[0]),  label=key[0])
    else:
        plt.plot([0] + value[0], [0.5] + value[1], color=col_map(key[0]), label=key[0])
plt.legend(loc="lower right")
# plt.xscale('log',basex=2) 
ax = plt.gca()
ax.set_xticks(ax.get_xticks()[::10])
plt.title("Root parallel by iterations")
plt.show()


with open('../../BayesianMCTS/experiments/final_bayesian_vl_gaussian/slurm-315775.out') as json_file:
    data = json.load(json_file)



processed_data = defaultdict(list)


for datapoint in data.values():
    # print("")
    # print(datapoint["data"])
    # [(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"]]
    processed_data[(datapoint["parallel_pattern"], datapoint["threads"])].append([(key, value['time_ms'], value["utility"]) for key, value in datapoint["data"].items()])

# Now average the data
it_processed_data = {}

for key, value in processed_data.items():
    it_processed_data[key] = average_std_its(processed_data[key])

# time_processed_data = {}
# for key, value in processed_data.items():
#     time_processed_data[key] = average_std_time(processed_data[key])


for key, value in it_processed_data.items():
    if key[0] == 3:
        plt.plot([0] + value[0], [0.5]+value[1], "--+", color=col_map(key[1]),  label=key[1])
    else:
        plt.plot([0] + value[0], [0.5]+value[1], color=col_map(key[1]),  label=key[1])
plt.legend(loc="lower right")
plt.title("Local Locking with No virtual loss")
ax = plt.gca()
ax.set_xticks(ax.get_xticks()[::10])
# plt.xscale('log', basex=2) 
plt.show()
