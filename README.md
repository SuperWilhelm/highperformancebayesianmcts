# Exploring Parallelism in Bayesian Monte-Carlo Tree Search

## Project information

**Student name:** John Wilhelm

**Student number:** s1644705

**EPCC supervisor name(s):** Dr Mark Bull & Dr Julien Sindt

## Description
This repository contains all files and information relating to John's Dissertation at EPCC 2021.

## Compiling the code for our experiments (For ARCHER2)
* The supporting software for this project is written in C++11 using MPI and OpenMP
* Clone the repository: `git clone https://git.ecdf.ed.ac.uk/msc-20-21/s1644705.git`
* Change directory into our repository: `cd s1644705`
* Checkout the commit of the experiment which you want to re-run (mapping given in the table below): `git checkout <<commit id>>`

| Experiment Batch | Experiment [Report Chapter]| SHA |
| ----------- | ----------- | ----------- |
| 1 | (a) Leaf parallel (with multiplicity)  [5.3] |  `9683c724aca87222bd68a844795371eedd532cdc`      |
| 2 | (a) UCT Leaf Parallel (without Multiplicity) [5.3],<br> (b) Root Parallel (all experiments excluding Normalization for UCT) [5.4], and<br> (c) the Tree Parallel (all) [5.5]  |  `dbb680dafb811d92f2a5b7cdd542806102f82839` |
| 3 | (a) Bayesian Leaf Parallel (without Multiplicity) [5.3], and <br> (b) UCT Root Parallel (with Normalization) [5.4]| `4a80915b11a3e80ac436f6966f7c038c8860689d` |
| 4 | (a) Speedup [5.6]|`cc9ae352acdc96223ae32c86b825db95458dc045` |
| 5 | (a) UCT-Treesplit [6.1]| `5d657646bc9d0a9d565f8cf845d03ec841752301`|

* Change directory into the code files `cd BayesianMCTS`
* Load the GNU compiler collection: `module restore PrgEnv-gnu`
* Load the Boost library: `module load boost`
* Compile: `CC -fopenmp main.cpp Planners/*.cpp Problems/*.cpp Problems/ProblemStates/*.cpp Planners/ThreadSafeUCTNodes/FrequentestUCT/*.cpp Planners/ThreadSafeUCTNodes/*.cpp Planners/*.h Problems/*.h Problems/ProblemStates/*.h Planners/ThreadSafeUCTNodes/FrequentestUCT/*.h Planners/ThreadSafeUCTNodes/*.h Planners/ThreadSafeUCTNodes/BayesianUCT/*.cpp Planners/ThreadSafeUCTNodes/BayesianUCT/*.h -o ParallelMCTS`
* The executable `ParallelMCTS` runs the experiments.
* One critical special case of this installation is that for the speedup experiments (4)(a), the threshold value must be set at compile time in lines `136` and `149` of the file `Planners/ParallelUCTPlanner.h`.

## Running experiment (For ARCHER2)
The general arguments for the `ParallelMCTS` executable are:

* (1) Planner: 0: "UCT", 1: "BayesianUCT-Gaussian", 2: "BayesianUCT-Numerical"
* (2) Planner parallelization: 0: "Serial", 1: "Leaf", 2: "Tree-Local-VL", 3: "Tree-Local-no_VL", 4: "Tree-Global-VL", 5: "Tree-Global-no-VL", 6: "Root-without-weight-sharing", 7: "Root-with-weight-sharing" 8: "UCT-Treesplit"
* (3) Planner iterations: Integer
* (4) random problem size: Integer
* (5) random problem min branching factor: Integer
* (6) random problem max branching factor: Integer
* (7) random problem generation seed: Integer

The results are printed in JSON format. The fields in the JSON are `threads` - the number of threads running the algorithm, `parallel_pattern` - what the planner parallelization strategy being used is,
`inference_mode` - either UCT, BayesianUCT-Gaussian or BayesianUCT-Numerical, `random_seed`: the random seed used to generate the test problem, `size`: The size of the MPI program and further describing the problem to be solved are
`min_branching_factor`, `max_branching_factor`, `problem_size`. Importantly the `data` field logs how the planner learned where the key is the completed iterations, and the values are the time of the logging point in *seconds* and the *ground-truth utility* of the action sequence selected at that point in the training. One exception is that for the speedup experiments, the first element of the `data` field is a tuple describing the simulations and wall clock time to reach the required optimality.

The specific arguments to re-run each of our experiments is given in the table below. Remember to set the number of Processing Elements you want to run the methods; for threaded elements, the number of threads will be taken as the environment variable `OMP_NUM_THREADS` and any parallel algorithms implemented using MPI will have their size determined by the SLURM submission job parameters.

| Experiment Batch | Command |
| ----------- | ----------- |
| 1 | (a) `./ParallelMCTS {0,1,2} 1 10000 1000 4 7 0`|
| 2 | (a) `./ParallelMCTS 0 1 10000 1000 4 7 0`,<br> (b) `./ParallelMCTS {0,1,2} {6, 7} 10000 1000 4 7 0` (BE),<br> (c) `./ParallelMCTS {0,1,2} {2,3,4,5} 10000 1000 4 7 0`|
| 3 | (a) `./ParallelMCTS {1,2} 1 10000 1000 4 7 0`,<br> (b) `./ParallelMCTS 0 7 10000 1000 4 7 0` (BE)|
| 4 | (a) `./ParallelMCTS 1 {1,3,7} 1000000 1000 4 7 0` (option 7 (BE))|
| 5 | (a) `./ParallelMCTS {0, 1} 8 256000 1000 4 7 0` (BE) (N.B. recall we do call MPI_Abort to terminate each run)|

(BE) denotes that even the test command must be ran on the Compute Nodes of ARCHER2 (given ARCHER2 does not support MPI on the login nodes). Here are submission scripts to run (2)(b) [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/master/compute_node_submission.slurm), (3)(b) [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/master/compute_node_submission_1.slurm), (4)(a) [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/master/compute_node_submission_2.slurm), and (5)(a) [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/master/compute_node_submission_4.slurm).

N.B. Remember to compile when you checkout a commit for the first time.

## Data Plotting
We have run precisely the above commands to run our experiments. Our experimental results are stored in this repository. 
The below script reads the results data and produces the final plots. The script is broken down into six main experiments for clarity. 
* Data processing .py scripts in "Data Processing" - [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/85df8c26bce85efacad746a5b7fc3c5c0ebe8cf8/Data%20Processing/summer_stage/final_plotter.py)


## Proposal Documents:
* Initial project proposal - [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/master/Initial_Proposal.pdf)
* Final project proposal/PP document - [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/master/PP_Report.pdf)

## Test script(s)
* We present one test script for testing our novel numerical integration approach for calculating moments of random variables with non-negative support - [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/blob/wip_run_final_root_leaf_tree_experiments/BayesianMCTS/main_test_approx.cpp)

## Meeting Logs:
* We keep track of key decisions made in our meetings and any actions for the week ahead - [Here](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/tree/master/Meeting%20Notes)

## Interesting Papers:
We note interesting papers we have read here, which may play a key role in the project. A full list of the papers making a final contribution to this dissertation is included in the references of the main paper.
* A key survey paper of MCTS methods: [MCTS Survey Paper](http://ccg.doc.gold.ac.uk/ccg_old/papers/browne_tciaig12_1.pdf)
* The Bayesian UCT algorithm: [Bayesian MCTS](https://arxiv.org/abs/1203.3519)
* Root, Tree and Leaf Parallelization: [Parallel MCTS overview](https://dke.maastrichtuniversity.nl/m.winands/documents/multithreadedMCTS2.pdf)
* MPI+OMP Large scale MCTS Parallelization: [UCT-Treesplit](https://ieeexplore.ieee.org/document/6876158)
* Cool (recent) large scale MCTS Application: [Molecular Design Application](https://arxiv.org/pdf/2006.10504.pdf)
* Root Parallelization statistics merge c.f. algo 1: [Parallelization of Monte Carlo Tree Search in
Continuous Domains](https://arxiv.org/pdf/2003.13741.pdf)


## Final Paper:
* Backups of our Latex files for the final paper are included in [this](https://git.ecdf.ed.ac.uk/msc-20-21/s1644705/tree/master/Report%20Backup) directory. 
