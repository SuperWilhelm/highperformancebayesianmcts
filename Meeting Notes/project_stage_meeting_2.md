# Meeting Minutes 1st of June 2021 (2 pm)

Present: John, Mark, Julien

## Actions
 
### New  
John:

(1) Explore poor performance of the Bayesian Algorithms.

(2) Obtain the Bayesian equivalent results to what has already been presented for the Frequentist algorithm's parallelization.
### On-going

(1) Try to remove the wastefulness of shared data seen in root parallelization - https://hal.inria.fr/inria-00512854/document Section 3 could be key here, which suggests depth limited averaging of the most visited (e.g. top 5% most visited nodes).
### Finished

(1) Some data was obtained and plotted. In particular, learning of tree parallelization method as virtual loss {on/off}, locking {global/local} and the number of threads varied {1,2,4,9,19,36} was plotted both vs iterations and wall clock time.

(2) Basic Tree parallelization performance data (vs iterations and time) was obtained and plotted. 
## Discussion
All agreed that coding up embarrassingly parallel algorithms in both MPI and OpenMP, although committed to in the project plan, is not a must if we consider ourselves to have better ideas to spend our time on. Mark told John to note that a mixture of methods, however, reduces the strength of conclusions that can be drawn from direct comparison experiments.

All other discussions centred around the results which John Presented:

- Speedup is not too interesting as is - Reduce simulation time to put more 'pressure' on the threaded section of the algorithm. Also, add some random variance (e.g. run filler code rather than just thread.sleep) to make the results more representative of real application.

- Root parallelization is (near-)useless as is. John stated that he knows of some improvements which could be made, which he will implement. Julien suggested pairwise combining of trees, which would be a novel approach for John to consider.

## DONM: 8th June at 2 pm. 

DONM: Tuesday next week

Weeks Until submission 11.5 weeks


