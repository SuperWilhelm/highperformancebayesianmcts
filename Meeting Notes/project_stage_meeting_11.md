# Meeting Minutes 10th August 2021 (2 pm)

Present: John, Julian

## Actions
 
For the next meeting. 
### Finished
    (1) The Project is ready for submission - some sections can, however, still be brushed up before we submit.
### New
    (1) John to act on Julian's feedback of Chapters 5, 6 and 7.
## Discussion
- Brief meeting this week given the time of the project we are at.
- Julien asked John how's best for him to help in the final stages, and John asked for him to review Chapters 5, 6 and 7 of his report. Julien kindly offered to do this in the next couple of days.
- John described how he will present his code to make re-running the experiments as easy as possible - by including SHAs of commits where the exact experiments were run along with listing how to call the executable. Julien liked this approach.
- John is hoping to submit the final project on Monday so that we can discuss the presentation in the meeting next Tuesday.
## DONM: 17th August at 2 pm. 

Weeks Until submission 1.5 weeks
