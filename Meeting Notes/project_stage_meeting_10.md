# Meeting Minutes 3rd August 2021 (2 pm)

Present: John, Mark

## Actions
 
For the next meeting. 
### Finished
    (1) John has finished his first complete draft of the submission and will send it to Mark and Julien in the next couple of days. 
### New
    (1) John to get the repository ready for submission
## Discussion
- John talked through the elements of the UCT-Treesplit algorithm, which he will include in this dissertation.
- Discussed that the Cache cannot be extended simply to the Bayesian setting with similar reasoning as to why we can only have a proxy formula for virtual loss. The statistics $\mu$ and $\sigma$ are updated through recursive computation from the bottom of the tree to the current node, and hence the statistics are not amenable to the simple tweaking hacks used for the average value stats in the UCT algorithm.
- Progressed to discuss one advantage of the Bayesian formulation over the standard UCT formula - with standard UCT, initial unguided threads can add noise to the average values. In contrast, the Bayesian version of this algorithm can simply ignore poorly guided searchers using its `max' operator. 
- Mark suggested that the max operator could also be used in the UCT algorithm to improve performance, and John agreed this is a good idea after considering some examples.
- John showed the results of the Bayesian-UCT-Treesplit experiment. John also advised that it would be a good idea to add error bars to speedup plots. 
- John is to work on the GitLab repository this week, mainly focusing on the reproducibility aspects of the project.
- John decided not to run any more experiments at this stage. 
- Mark to miss the next meeting but happy to give draft feedback via email. 
## DONM: 10th August at 2 pm. 

Weeks Until submission 2.5 weeks