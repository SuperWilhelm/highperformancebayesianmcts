# Meeting Minutes 29th of June 2021 (2 pm)

Present: John, Julien
Absent: Mark

## Actions
 
For the next meeting. 
### On-going
    (1) Try to write up to page 35 by the meeting next week so that that focus can shift to the UCT-Treesplit algorithm for the final few weeks. 
### Finished
    (1) Fix issues with the results presented so far in the meetings (e.g. reduce simulation time to make locking patters perform more distinctly, mock code for simulation, each thread with private random number generator) for final presentation
    (2) Wrote Python code to generate plots for the report. Entered the new plots into the final report. 
    (3) Some redrafting of the first four chapters. 
## Discussion
- General discussion regarding John taking many days for interviews etc., in the last two weeks off for other work. Julien was happy with this.
- Discussed problems seen with leaf parallel's poor scalability in the UCT case - This is alarming since giving a serial algorithm better input data (in this case through using parallelism) should not make the algorithm perform worse. This implies that counting parallel leaf simulations with multiplicity is a bad idea. John will revert this.
- John discussed the changes he has made to the code to make experimental results more realistic since the last meeting. Firstly mock code, which has been discussed extensively now implemented. Secondly, each thread now has its own random number generator.
- John showed the plots which he had generated of (iterations/time) vs learning. Discussion centred around changing plots to (\alpha-optimality) vs (time/iterations to reach).
- Julien detailed to John how to modify code to run speedup experiments - run until goal reached (e.g. 95% optimality and record time) rather than running for a fixed length of iterations.
- Julien offered to send John a Slurm script to run this experiment for 1,2,4,...,64 threads all in one submission to ARCHER2. 
- John advised to focus more on report writing at this stage of the project. John fully agreed.


## DONM: 6th July at 2 pm. 

Weeks Until submission 7.5 weeks
