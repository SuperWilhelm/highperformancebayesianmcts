# Meeting Minutes 2021-03-10

Present: John, Mark, Julien

## Actions
 
### New  
    John: Complete first draft of pp report
        (by the next meeting)
### Finished
    John:
        (1) Finished the experiments for the project preperation document.

## Discussion
- John showed grahps of first 2 experiments - (1) Bayesian UCT numerical vs Bayesian UCT Gaussian vs UCT and (2) UCT leaf parallelism as number of threads varied.
- Julien suggests John to should investigate why there remains notacable noise for 38 threads even after averaging over 100 runs.
- Mark suggested John to plot subset of results to get more information.
- Hybrid models decided to be of interest in this project, where results of initial experiments could determine cut of points for nested Parallelizm. 
- John to experiment with linear log (x,y) axis to make results clearer. - Additionally Julien would be interested as a reader to see standard dev. plotted. 
- John told to focus now on the project preparation document and leave the software development for now since this is the priority.
## DONM: 17th March 16:00. 
