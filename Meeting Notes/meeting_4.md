# Meeting Minutes 2021-03-03

Present: John, Mark, Julien

## Actions
 
### New  
    John: Run the experiments for the pp report described in the current pp report draft. Process and plot results.
        (by the next meeting)
### Overdue
    John: Write comprehensive unit tests for the codebase developed so far in the project.
        (by the next meeting)
### Finished
    John:
        (1) Wrote 8 pages of first draft of pp report.

## Discussion

- Supervior meetings to run weekly until the end of semester.
- Discussed using the Mandelbrot code from Threaded Programming as a `pure compute' code for mocking simulation and code from CW1 as a memory bound example code. Discussed that it would be good in dissertation to consider simulation codes at both extremes of this spectrum.
- General feedback for the pp report so far was offered to John and he will update draft to reflect comments.  
## DONM: 10th March 16:00. 
