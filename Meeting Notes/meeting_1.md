# Meeting Minutes 2021-01-20

Present: John, Mark, Julien

## Actions
 
### New  
    John:
        (1) Will write motivation section for the project in the Project Preparation report.
        (2) Will implement code to generate random problems, parameterized by random seed for reproducability.
        (3) Will read Reinforcement Learning dissertation from last year. 
                - "Designing and Evaluating AI for Air Traffic Control" recomended by JS. 
    by the next meeting. 
### On-going
    John:
        (1) Will complete the implementation of the Gaussian Approximation and Numerical integration method.
    by the next meeting. 
### Finished
    John:
        (1) Implementation of vanilla MCTS

## Discussion

- JW described one potential application of the MCTS algorithm in order to illustrate a very high level view of the different stages of the algorithm and potential challanges with parallelization.
    - Discussed "virtual loss" (assuming imcomplete iterations are going to return terrable values i.e. 0) as an entry point into parallelizing iterations.

- MB encouraged JW that success metrics and test domains should be developed as a priority.
    - JW briefly described a simple domain used in the origional Bayesian MCTS paper, which will be again an excelent 'measure stick' for the different methods reviewed in this investigation.
    - JW to implement as a priority in PP.
    
- JS and MB commented that JW should not only focous on hardware based performance metrics but also algorithmic performance metrics (e.g. loss vs runtime and loss vs iteration count are both of interest).
    - JW agreed that he had overlooked this point and took desciscion for Numerical Integration methods to remain central in the research.

- JW suggested that the general trajectory of the project should be to get initial results on single node setup, before progressing to more challanging shared memory setup, to first ensure MVP before taking more risky directions
    - Inital results for single node should be included in PP report to establish the "Proof of Concept" component of the PP module. 
    - MB indicated that one alternative route to distributed memory would be to use experimental hardware with massive amounts of Non-volitile memory.
    - JW mentioned he had seen an MPI + OpenMP hybrid algorithm (UCT-Treesplit) in this domain which he would be interested in implementing.
    - Breif discussion on potential to implement using single sided message passing. MB cautioned JW of the potential software development overhead of taking this direction.


Thanks to JS who sent his detailed meeting notes to JW.
## DONM: 3rd Febuary 16:00. 
