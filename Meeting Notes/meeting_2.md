# Meeting Minutes 2021-02-03

Present: John, Mark, Julien

## Actions
 
### New  
    John:
        Nail down exact experiments to be performed, and use this to estimate compute resources required for this
        dissertation.
        (by the next meeting)
### Overdue:
    John:
        (1) Will complete the implementation of the Gaussian Approximation and Numerical integration method.
        (by the next meeting)
### On-going
    John:
        (1) Continue writing report.
### Finished
    John:
        (1) Rough draft of first 3 pages of pp report.
        (2) Test cases implemented, along with pretty printing of the examples.
        (3) Dissertation report from the passed read. 

## Discussion

- JW described a paper describing an application of the project in Chemistry. All agreed that a concrete application
  would be good to mention briefly in the dissertation report. 

- JS asked JW for more specific goals of the project which was only partially answered. MB suggested that JW should write
  idelised abstract which he would like at the end of the dissertation in order to get ideas flowing. JW liked the recomendation
  and will do just that.
  
- JW outlined that he thinks a good high level plan for the project would be for the first month to focus on shared memory
  experimentation, and the second month on distributed. JS and MB told JW that on the surface that seems a reasonable 'ideal'
  trajectory for the project.

## DONM: 17rd February 16:00. 