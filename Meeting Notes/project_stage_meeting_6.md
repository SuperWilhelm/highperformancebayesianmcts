# Meeting Minutes 6th of July 2021 (2 pm)

Present: John, Julien
Absent: Mark

## Actions
 
For the next meeting. 
### On-going
    (1) The final, crucial implementation task remaining is implementing the UCT Treesplit Algorithm with MPI+OpenMP. John is to attempt to obtain this implementation this week. 
### Finished
    (1) The plots for the first experiments chapter (Root, Leaf and Tree Parallelization) are completed and nicely formatted.
    (2) The first new addition to this collection of graphs this week was: a speedup experiment comparing the parallelization methods for the Bayesian Gaussian algorithm learning to 99% and 99.9% optimality. Secondly was an experiment comparing the effects of using local locking vs global locking in the Bayesian Gaussian method. Additionally, data was added to existing plots to show the impact of counting with multiplicity of simulations in UCT Leaf Parallelization.
## Discussion

- Discussion of why counting without multiplicity should, when averaged over many runs, should cause a non-decreasing performance pattern as the number of processes increases. Considered this as entering better data into the serial algorithm
- Discussion of why counting with multiplicity, may yield scaling problems. due to the exploration term in $\sqrt{\ln{n}/n_j}$ vanishing more rapidly if there is a constant factor of e.g. $k=128$, before each $n$ value.
- John told that he should include such arguments in his dissertation.
- John described that he will now be using the performance costs of wall-clock time and (Simulations/# of processing elements) to present the results most clearly.
- John showed some plots showing the breaking down of the numerical representation of the numerical method when distributions become extremely sharp due to 10,000s of results. John was encouraged to test his hypothesis with some basic experiments.
- John presented the speedup plots for the Bayesian Gaussian algorithm learning to 99% and 99.9% optimality with tree/leaf/root parallelization.
- John presented the lock location experiments showing that in (Simulations/# of processing elements) being more faithful to the serial algorithm with global locks is benificial, however, in wall clock time, making the stronger assumptions (local locks) in order to exploit more parallelism is seen to be beneficial.
- Julian offered John help with single-sided MPI via email this week if he needs it.  

## DONM: 13th July at 2 pm. 

Weeks Until submission 6.5 weeks
