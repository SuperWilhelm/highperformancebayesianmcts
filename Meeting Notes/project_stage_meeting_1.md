# Meeting Minutes 25th of May 2021

Present: John, Mark, Julien

## Actions
 
### New  
    John:
        (1) John committed to having some first data by the meeting in one weeks time.
        (2) John to clean up code to make sure code developed at the start of the project (e.g. for inference methods/thread saftey) can be plugged into the UCT-Treesplit with little modification.
        (3) John to develop Root Parallelization (using MPI), tree and leaf paralyzation (using OMP) with standard and bayesian inference in the next week.
### On-going
    Nothing since it is the first meeting back
### Finished
    Nothing since it is the first meeting back
## Discussion
JW asked for help on how is best to log the progress of a threaded algorithm as he had never done it before - was concerned that logging would significantly affect results and considered whether an additional thread is needed for this task. John was advised that a straightforward approach is to perform logging infrequently on the main thread. JS suggested to John if he is worried about such logging affecting results, then simply run a quick experiment to see if this is the case.


## DONM: 1st June at 2pm. 

DONM: Tuesday next week

Weeks Until submission 12.5