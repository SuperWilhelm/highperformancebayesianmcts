# Meeting Minutes 2021-02-17

Present: John, Mark, Julien

## Actions
 
### New  
    John: Write comprehensive unit tests for the codebase developed so far in the project
        (by the next meeting)
    John: Write several more pages for the project prep document. 
        (by the Monday before the next meeting)
### Finished
    John:
        (1) Experiment specification considered in detail.
        (2) All three solvers for investigation in this project implemented

## Discussion

- JW described current implementation and test examples achieved and MB suggested that it would be useful to further
   consider: (a) How will time cost of the simulaton stage of the algorithm be mocked in the experiments? - Running some `dummy' code would be more convincing than sleeps. (b) Could a game actually be implemented to make experiments more faithful to a real world example.
           
- JS asked JW some questions about his hypothesis for the experiments to be undertaken. JW was not sure and this should be considered further.

- MB gave JW some tips as to how to best make the search space he is considering more tractable, for example, taking a greedy approach only exploring the best options as experiments progress. 
## DONM: 3rd March 16:00. 