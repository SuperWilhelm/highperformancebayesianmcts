# Meeting Minutes 27th of July 2021 (2 pm)

Present: John, Julien

## Actions
 
For the next meeting. 

### On-going
    (1) Key focus this week is on improving the current state of the dissertation report draft (e.g. re-drafting/adding additional information). 
## Discussion
- John talked through the elements of the UCT-Treesplit algorithm, which he will include in this dissertation. Julien understood the method after some questions.
- Both discussed that the Cache system could (and probably should) be put beyond the project's scope at this stage.
- Discussed that the Cache cannot be extended simply to the Bayesian setting.
- Julien gave John some tips regarding how to reference copied figures properly. Be explicit, as with long captions, confusion can arise.
- John to focus on finishing off the report this week. Julien kindly offered to review the draft if given one weeks notice. 
- Finally, Julien told John that it's a good idea to start thinking about the presentation at this stage. Tip: focus on highlights rather than trying to cram everything in. 
- Meeting finished early since both were happy - Julien to miss next week - Mark should be back.
## DONM: 3rd August at 2 pm. 

Weeks Until submission 3.5 weeks
