# Meeting Minutes 20th of July 2021 (2 pm)

Present: John, Julien, Mark

## Actions
 
For the next meeting. 
### New:
    (1) Key focus this week is on improving the current state of the dissertation report draft (e.g. re-drafting/adding additional information). 
### On-going
    (1) UCT-Treesplit Algorithm switched from single-sided message passing (which had a difficult to solve deadlock problem). Implementation still has some issues (poor performance with Cache system turned on, which surely should improve performance). John to investigate these issues the week after this. Sensible to focus more on write up at this stage.
### Finished
    (1) Ran experiment for Bayesian Leaf Parallelization without multiplicity. - Found scalability was overall worse. 
    (2) Ran experiment where shared visit counts in Root Parallelization are normalized by the number of processes. - This optimization fails to beat the root parallelization method without any sharing at all!
    (3) Ran test of the error which numerical integrator incurs as the distributions become more extreme - Test file created and added to the README's unit test section. We do see a large (~%80 percentage error from our integration method for extremely sharp functions, i.e. Beta(10^6, 10^6))
    (4) Ran Leaf parallelization on 65 threads to evaluate whether the increased (time) of this method may be due to passing over a socket - ran with 96 threads and still observed runtimes as expected. Socket crossing is not the issue.
    (5) Wrote section in report about the UCT-Treesplit Algorithm - Included two diagrams to help with the explanation.
    (6) Wrote a section in the report about our implementation of the UCT-Treesplit Algorithm - Wrote about new single-sided implementation.
## Discussion
- John presented the results of the interesting experiments decided on in the previous meeting. Firstly, he showed he found that in contrast to the UCT case, Bayesian-UCT scales better when counting state visits with multiplicity. Quite a surprising result, however, in the case of the Bayesian-UCT algorithms, we are working with approximations of continuous functions at scale, making reasoning difficult.
- Discussed normalization of root parallelization weight sharing. John normalized the shared visit counts by the number of processes while noting a problem with scaling up the bottom of the tree. Julien and Mark suggested a different normalization (scaling down the bottom of the tree) scheme, but there is not much reason to suggest this method will perform any better. John to report back his interest in this method this week
- John presented a numerical integration test script - Importantly, it was concluded that it is not necessary to plot this data in the report.
- All together inspected the distribution of the 128 thread Leaf-UCT data seeing min values as expected of around 2.5 seconds but some runs up to 2 mins+ runtime - Mark suggested this may be due to the experiment being affected by a background process on the node on a subset of the runs - no further investigations to take place on this front. Results, as is, can be reported/removed from the report. 
- In the final minutes of the meeting, John tried to explain his partial progress with the UCT-Treesplit algorithm, but Julien didn't fully understand his rushed explanations. 
- Mark and Julien encouraged John to focus on the big picture of the project and shift his focus to writing up at this stage. John agreed that this is a great suggestion.
- Mark will miss the next meeting.
## DONM: 27th July at 2 pm. 

Weeks Until submission 4.5 weeks
