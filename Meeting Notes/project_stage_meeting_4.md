# Meeting Minutes 15th of June 2021 (2 pm)

Present: John, Mark, Julien

## Actions
 
### New  
    (1) Decide on the exact experiments/argument to make for the Root/Tree/Leaf parallel methods' experiments section.
    (2) Fix issues with the results presented so far in the meetings (e.g. reduce simulation time to make locking patterns perform more distinctly) for the final presentation
    (3) Finally, write up the next 15 pages of the report with all of this data.
For the next meeting. 
### On-going
    (1) Report writing. 
### Finished
    (1) Rough draft of first 20 pages achieved.
## Discussion
- John sent over what he had achieved so far with the write-up, and it was agreed at a high level it looked like a reasonable start.
- John spoke over some of the maths and some of the figures he has decided to include so far.
- Main discussions centred around the presentation of the data.
    - One suggestion of Mark was to have plots of \alpha vs {time/iterations} with each line representing an algorithm (i.e. planner with inference method and threads/size fixed)
- Next meeting is to be skipped (discussed over email) due to holidays and interviews, which John has next week. 
## DONM: 29th June at 2 pm. 

Weeks Until submission 9.5 weeks