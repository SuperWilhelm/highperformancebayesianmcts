# Meeting Minutes 13th of July 2021 (2 pm)

Present: John, Julien, Mark

## Actions
 
For the next meeting. 
### New:
    (1) Run experiment for Bayesian Leaf Parallelization without multiplicity
    (2) Run experiment where shared visit counts in Root Parallelization are normalized by the number of processes.
    (3) Run test of the error which numerical integrator incurs as the distributions become more extreme.
    (4) Run Leaf parallelization on 65 threads to evaluate whether the increased (time) of this method may be due to passing over a socket.
    (5) Write section in report about the UCT-Treesplit Algorithm.
    (6) Write a section in the report about our implementation of the UCT-Treesplit Algorithm.
### On-going
    (1) Investigate deadlock occurring in our implementation of the UCT-Treesplit Algorithm. 
### Finished
    (1) Implemented the UCT-Treesplit Algorithm and the node Cache system. It runs without deadlock on two processes, but for more than two processes, a deadlock occurs.
## Discussion
- Meeting focused on a discussion of the results obtained so far.
- New items (1)-(4) describe the key takeaways from this discussion as the next steps. 
- John advised to check that two near-identical looking lines in two distinct plots were as they should be and not due to a data processing error - this has been checked.
- Discussions were mainly focused on why the observed results are as they are. John will now write up the explanations which everyone thought were feasible in his report.
- John gave an update on the progress of the UCT-Treesplit implementation, in that progress on this section of the project was going well - John will describe the algorithm and his implementation next week. 

## DONM: 20th July at 2 pm. 

Weeks Until submission 5.5 weeks