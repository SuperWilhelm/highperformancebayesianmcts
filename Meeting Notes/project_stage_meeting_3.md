# Meeting Minutes 8th of June 2021 (2 pm)

Present: John, Mark, Julien

## Actions
 
### New  
John:
    (1) Main focus this week is pushing forward with writing the dissertation document first 2/3 chapters.
    (2) Consider the counter-intuitive results observed for the Bayesian-Virtual Loss (Tree Parallel) experiment.
### On-going
    None
### Finished
    (1) Explore poor performance of the Bayesian Algorithms. - solved - Backpropogating the posterior predictive rather than just the posterior!
    (2) Obtain the Bayesian equivalent results to what has already been presented for the Frequentist algorithm's parallelization. - Achieved through adding Bayesian nodes to the codebase.
## Discussion
Discussion centred around the results for parallelization of the Bayesian:
- Leaf parallelization - All seemed reasonable.
- Root parallelization - For the most frequent algorithm when statistic sharing for the top three levels of the tree, this causes problems for the long term training of the algorithm. Additionally, we should next investigate the cost (in terms of wall-clock time) for performing the additional All to All synchronizations.
- Root parallelization - Gaussian Approach - Statistic sharing happens bottom-up to avoid shared information being overwritten on the first back-prop. No long term training issue as seen previously, since with bayesian formulation, inconsistent visit count data is not considered in the forward-prop stage.
- Tree parallelization - Results are confusing as is. Why does the version with perfect information (i.e. serial algorithm) have the worst performance for large sections of the training?

A quick discussion was had relating to unit testing, which MB suggested to JW would likely not be too useful based on the current state of the project, which JS agreed with.

Some discussion over JW deviating from how the Bayesian paper recommends a calculation to be performed since JW feels he has a better method using the observation in https://stats.stackexchange.com/questions/10159/find-expected-value-using-cdf allowing for mean and standard deviation to be calculated in one pass of the CDF, with the computation of max distribution collapsing to elementwise multiplication of arrays. 
## DONM: 15th June at 2 pm. 

Weeks Until submission 10.5 weeks

