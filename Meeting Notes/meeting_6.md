# Meeting Minutes 2021-03-17

Present: John, Mark, Julien

## Actions
 
### New
    John:
        (1) Prepare pp and gitlab repository for submission on Friday week 10. Then submit
    Mark and Julien:
        (1) To send John some feelback to advise him of any final changes to the pp report before he submits.
### Finished
    John:
        (1) Finished all but one sub-section of the pp-report

## Discussion
- John reported back that he investigated `noisy grahps' issues and found that experiment was repeated only 25 times and not 100 due to typo in Slurm script. 
- Discussed Gantt chart. John recomended to decompress the content in the first month. Additionally, planning down to the day not really convincing so this could be updated.
- Discussed extra work if time allows - John told to consider that GPU programming is really tricky so this could be a factor when deciding, if the situation arises, what is the best `extra' task to focus on first. 
- Python interfacing work not really that inpactful in that the primary focous should be on research and not software engineering. 
## DONM: To be discussed over email after the May exams are completed - John to initiate.