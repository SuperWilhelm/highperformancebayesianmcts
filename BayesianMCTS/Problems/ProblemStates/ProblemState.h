#ifndef BAYESIANMCTS_PROBLEMSTATE_H
#define BAYESIANMCTS_PROBLEMSTATE_H
#include <map>

class ProblemState {
private:
public:
    int state_id;
    int is_terminal;
    explicit ProblemState(int state_id, int is_terminal);
    double minimax_value = -1.0;
    double average_value = -1.0;

    virtual void compute_minimax_value();
    virtual void compute_average_value();
    virtual std::tuple<double, double> get_terminal_data();
    virtual int get_child_count_to_depth(int depth);
    bool terminal();

    std::map<int, ProblemState*> children = {};
};

#endif
