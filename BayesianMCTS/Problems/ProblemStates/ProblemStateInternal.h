#ifndef BAYESIANMCTS_PROBLEMSTATEINTERNAL_H
#define BAYESIANMCTS_PROBLEMSTATEINTERNAL_H
#include <vector>
#include <map>
#include "ProblemState.h"

class ProblemStateInternal: public ProblemState {
private:
    int next_action_index = 0;
public:
    explicit ProblemStateInternal(int state_id);
    void add_child(ProblemState *new_child);
    void compute_minimax_value() override;
    void compute_average_value() override;
    std::tuple<double, double> get_terminal_data() override;
    int get_child_count_to_depth(int depth) override;
};

#endif
