#include "ProblemStateLeaf.h"

ProblemStateLeaf::ProblemStateLeaf(int state_id) : ProblemState(state_id, 1) {}

void ProblemStateLeaf::compute_minimax_value() {
    ProblemState::compute_minimax_value();
}

void ProblemStateLeaf::compute_average_value() {
    ProblemState::compute_average_value();
}

std::tuple<double, double> ProblemStateLeaf::get_terminal_data() {
    return ProblemState::get_terminal_data();
}

int ProblemStateLeaf::get_child_count_to_depth(int depth){
    return ProblemState::get_child_count_to_depth(depth);
}
