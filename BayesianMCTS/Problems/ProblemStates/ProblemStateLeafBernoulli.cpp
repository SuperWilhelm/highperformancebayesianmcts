#include "ProblemStateLeafBernoulli.h"
#include <iostream>
#include <thread>
#include <chrono>

#include <math.h>
#define NPOINTS 20
#define MAXITER 20

//extern volatile double diff;

#include <omp.h>

ProblemStateLeafBernoulli::ProblemStateLeafBernoulli(int state_id, double p): ProblemStateLeaf(state_id) {
    this->p = p;
}

double ProblemStateLeafBernoulli::sampleReward(std::mt19937 *mt) {
    std::bernoulli_distribution dist(this->p);
    double start = omp_get_wtime();
    double a[NPOINTS][NPOINTS], b[NPOINTS][NPOINTS], x[NPOINTS][NPOINTS];
    double diff = 0;
    double tmp = 44;
    for (int i=0; i<NPOINTS; i++) {
       for (int j=0; j<NPOINTS; j++) {
          a[i][j] = 1.0;
          b[i][j] = 3.14159265359;
          x[i][j] = 1.0;
       }
    }

    for (int iters = 0; iters < MAXITER; iters++){
      for (int i=1; i<NPOINTS; i++) {
        for (int j=0; j<NPOINTS; j++) {
          tmp = a[i][j]/b[i-1][j];
          x[i][j] -= x[i-1][j] * tmp;
          b[i][j] -= a[i][j] * tmp;
        }
      }

      diff = 0.0;

      for (int i=0; i<NPOINTS; i++) {
        for (int j=1; j<NPOINTS; j++) {
          tmp = a[i][j]/b[i][j-1];
          x[i][j] -= x[i][j-1] * tmp;
          b[i][j] -= a[i][j] * tmp;
          diff += fabs(a[i][j] * tmp)/(NPOINTS*NPOINTS);
        }
      }
    }
    double finish = omp_get_wtime();
//    printf("Time = %12.8f seconds\n",finish-start);
    return dist(*mt);
}

void ProblemStateLeafBernoulli::compute_minimax_value() {
    this->minimax_value = this->p;
}

void ProblemStateLeafBernoulli::compute_average_value() {
    this->average_value = this->p;
}


std::tuple<double, double>ProblemStateLeafBernoulli::get_terminal_data(){
    return {1, this->state_id};
}


int ProblemStateLeafBernoulli::get_child_count_to_depth(int depth){
    return 1;
}
