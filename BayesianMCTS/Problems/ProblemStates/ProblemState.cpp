#include "ProblemState.h"
#include <iostream>
ProblemState::ProblemState(int state_id, int is_terminal){
    this->state_id = state_id;
    this->is_terminal = is_terminal;
}

void ProblemState::compute_minimax_value() {}

void ProblemState::compute_average_value() {}

std::tuple<double, double> ProblemState::get_terminal_data(){
    return {-2, -2};
}

int ProblemState::get_child_count_to_depth(int depth){
    return -1;
}

bool ProblemState::terminal() {
    return this->is_terminal;
}
