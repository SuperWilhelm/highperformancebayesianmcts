#include "ProblemStateInternal.h"
#include <iostream>
ProblemStateInternal::ProblemStateInternal(int state_id) : ProblemState(state_id, 0) {}

void ProblemStateInternal::add_child(ProblemState *new_child) {
//    std::cout << "Adding child to state: " <<  state_id << " w/ address " << this << " New Child [" << this->next_action_index << "] = " << new_child << std::endl;
    this->children[this->next_action_index++] = new_child;
}

void ProblemStateInternal::compute_minimax_value() {
    std::map<int, ProblemState*>::iterator it;
    for (it = this->children.begin(); it != this->children.end(); it++){
        (it->second)->compute_minimax_value();
    }
    //Use the data in the children to compute this value
    double max_value = -1.0;
    for (it = this->children.begin(); it != this->children.end(); it++){
        if(it->second->minimax_value > max_value){
            max_value = it->second->minimax_value;
        }
    }
    this->minimax_value = max_value;
}

void ProblemStateInternal::compute_average_value() {
    std::map<int, ProblemState*>::iterator it;
    for (it = this->children.begin(); it != this->children.end(); it++){
        (it->second)->compute_average_value();
    }
    //Use the data in the children to compute this value
    double running_total_reward = 0.0;
    for (it = this->children.begin(); it != this->children.end(); it++){
        running_total_reward += it->second->average_value;
    }
    this->average_value = running_total_reward / this->children.size();
}


std::tuple<double, double> ProblemStateInternal::get_terminal_data(){
    int lowest_terminal_id = -1;
    int terminal_count = 0;
    std::map<int, ProblemState*>::iterator it;
    for (it = this->children.begin(); it != this->children.end(); it++){
        std::tuple<double, double> child_data = (it->second)->get_terminal_data();
        if (lowest_terminal_id == -1 || std::get<1>(child_data) < lowest_terminal_id){
            lowest_terminal_id = std::get<1>(child_data);
        }
        terminal_count += std::get<0>(child_data);
    }

    return {terminal_count, lowest_terminal_id};
}

int ProblemStateInternal::get_child_count_to_depth(int depth){
    if (depth == 0) return 1;
    int rtn = 1;
    std::map<int, ProblemState*>::iterator it;
    for (it = this->children.begin(); it != this->children.end(); it++){
        rtn += (it->second)->get_child_count_to_depth(depth - 1);
    }

    return rtn;
}
