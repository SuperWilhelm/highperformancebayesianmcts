#ifndef BAYESIANMCTS_PROBLEMSTATELEAF_H
#define BAYESIANMCTS_PROBLEMSTATELEAF_H

#include <random>
#include "ProblemState.h"

class ProblemStateLeaf : public ProblemState{
public:
    ProblemStateLeaf(int state_id);
    void compute_minimax_value() override;
    void compute_average_value() override;
    std::tuple<double, double> get_terminal_data() override;
    int get_child_count_to_depth(int depth) override;
};

#endif
