#ifndef BAYESIANMCTS_PROBLEMSTATELEAFBERNOULLI_H
#define BAYESIANMCTS_PROBLEMSTATELEAFBERNOULLI_H
#include <random>
#include "ProblemStateLeaf.h"

class ProblemStateLeafBernoulli: public ProblemStateLeaf {
    double p;
public:
    ProblemStateLeafBernoulli(int state_id, double p);
    double sampleReward(std::mt19937 *mt);

    void compute_minimax_value() override;
    void compute_average_value() override;

    std::tuple<double, double> get_terminal_data() override;    
    int get_child_count_to_depth(int depth) override;
};

#endif
