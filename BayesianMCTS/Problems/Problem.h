#ifndef BAYESIANMCTS_PROBLEM_H
#define BAYESIANMCTS_PROBLEM_H

#include <random>
#include "ProblemStates/ProblemState.h"
#include "ProblemStates/ProblemStateInternal.h"

class Problem {
private:
    int seed;
    std::mt19937 mt{};
protected:
    ProblemStateInternal *root;
public:
    Problem(ProblemStateInternal *problem_state, int seed);
    ProblemStateInternal* get_start_state();
    double get_state_minimax_value(ProblemState* problem_state);
    double get_state_average_value(ProblemState* problem_state);

    void re_seed(int new_seed);

    static Problem* generate_random(int seed, int min_branching_factor, int max_branching_factor,
                                    int no_nodes);

};

#endif
