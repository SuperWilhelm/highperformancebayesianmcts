#include "Problem.h"
#include "ProblemStates/ProblemStateLeaf.h"
#include "ProblemStates/ProblemStateLeafBernoulli.h"
#include <iostream>

Problem::Problem(ProblemStateInternal *root_state, int seed) {
    this->root = root_state;
    this->seed = seed;
    this->mt.seed(seed);
}

ProblemStateInternal* Problem::get_start_state() {
    return this->root;
}

void Problem::re_seed(int new_seed){
    this->mt.seed(new_seed);
}

double Problem::get_state_minimax_value(ProblemState *problem_state) {
    return problem_state->minimax_value;
}

double Problem::get_state_average_value(ProblemState *problem_state) {
    return problem_state->average_value;
}

// *** Below is code to generate a random problem ***
int random_children_count(int min_branching_factor, int max_branching_factor, std::mt19937* mt){
    std::uniform_int_distribution<int> dist(min_branching_factor, max_branching_factor);
    int rtn =  dist(*mt);
    return rtn;
}

ProblemStateLeaf* random_terminal_node(int node_id, std::mt19937* mt){
    std::uniform_real_distribution<double> dist(0, 1);
    double p = dist(*mt);
    return new ProblemStateLeafBernoulli(node_id, p);
}

Problem* Problem::generate_random(int seed, int min_branching_factor, int max_branching_factor, int no_nodes) {
    std::vector<ProblemStateInternal*> non_terminals_for_expansion = std::vector<ProblemStateInternal*>();

    auto* root = new ProblemStateInternal(0);
    auto* problem = new Problem(root, seed);

    non_terminals_for_expansion.push_back(root);
    std::vector<ProblemStateInternal*>::iterator it;
    std::vector<ProblemStateInternal*> nodes_for_expansion_next_level = std::vector<ProblemStateInternal*>();

    int node_count = 1;
    while(node_count < no_nodes){
        // Loop over the open nodes at the current depth
        for (it = non_terminals_for_expansion.begin(); it != non_terminals_for_expansion.end(); ++it){
            // Generate random number describing the number of children to add
            int no_children = random_children_count(min_branching_factor, max_branching_factor, &problem->mt);
            if (no_children > no_nodes - node_count){
                no_children = no_nodes - node_count;
            }
            if (no_children <= 0){
                (*it)->add_child(random_terminal_node(node_count++, &problem->mt));
                continue;
            }
            for (int new_child_index = 0; new_child_index < no_children; new_child_index++){
                auto* new_problem_state = new ProblemStateInternal(node_count++);;
                (*it)->add_child(new_problem_state);
                nodes_for_expansion_next_level.push_back(new_problem_state);
            }
        }
        non_terminals_for_expansion = std::move(nodes_for_expansion_next_level);
    }

    for (it = non_terminals_for_expansion.begin(); it != non_terminals_for_expansion.end(); ++it){
        (*it)->add_child(random_terminal_node(node_count++, &problem->mt));
    }

    problem->root->compute_minimax_value();
    problem->root->compute_average_value();

    return problem;
}


