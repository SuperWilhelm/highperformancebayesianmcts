#include "UCTTreesplit.h"
// TODO: This section will be implemented in the second half of the project
// Some notes:
// Use non-blocking to start with
// Remember there are two versions of this papaer:
// https://link.springer.com/content/pdf/10.1007%2F978-3-642-23397-5_36.pdf
// Figure 4 actually makes sense after some thought - Update msg will go to all nodes on the search thread
// Note: Every PE has the root node!
// Start by simply hashing the root node => Root is on rank zero, this makes starting of new sims trivial to derermine
