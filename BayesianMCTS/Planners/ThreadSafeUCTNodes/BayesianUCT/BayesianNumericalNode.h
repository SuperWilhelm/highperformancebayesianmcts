
#ifndef BAYESIANMCTS_BAYESIANNUMERICALNODE_H
#define BAYESIANMCTS_BAYESIANNUMERICALNODE_H

#include "../UCTNode.h"
#include <list>
class BayesianNumericalNode: public UCTNode  {
    void initialize_children();
    double* distribution = nullptr;
public:
    BayesianNumericalNode(ProblemState* game_state,  bool access_lock);
    BayesianNumericalNode() = default;
    double mean = 0.5;
    double standard_dev = 0.28867513459;

    double alpha = 1.0;
    double beta = 1.0;

    std::map<int, BayesianNumericalNode*> children = {};

    void update_statistics(double new_reward, int weight, int store_local_proc_results);
    BayesianNumericalNode* select_ucb_child(int virtual_loss, int weight);
    std::tuple<int, BayesianNumericalNode*> select_optimal_child();

    std::tuple<double, double> get_statistics_for_global_sum();
    void update_statistics_from_global_sum(double global_total_visits, double global_total_reward);

    void get_terminals(std::list<BayesianNumericalNode*>* rtn);
    void get_shallow_nodes(int depth, std::list<BayesianNumericalNode*>* rtn);

    void global_backprop();

    int share_type = -1;
    int force_global_bprop = 1;
};

#endif
