#include "BayesianGaussianNode.h"
#include <vector>
#include <iostream>
#include <cfloat>
#include <cmath>

// Define Gaussian Helper Functions
std::tuple<double, double> compute_max_distribution(std::vector<std::tuple<double, double>> *children_statistics);
std::tuple<double, double> compute_pairwise_max(double mu_1, double mu_2, double sigma_1, double sigma_2);
double F1(double alpha);
double F2(double alpha);
double normal_cdf(double alpha);
double normal_pdf(double alpha);


BayesianGaussianNode::BayesianGaussianNode(ProblemState *game_state, bool access_lock) : UCTNode(game_state,
                                                                                                 access_lock) {
    this->initialize_children();
}

void BayesianGaussianNode::update_statistics(double new_reward, int weight, int maintain_local_statistics) {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    if(maintain_local_statistics){
        this->local_proc_total_visits += weight;
        this->local_proc_total_reward += new_reward;
    }

    this->no_visits += weight;
    this->open_no_visits -= weight;

    if(!this->is_terminal){
        // Perform backup from children
        std::vector<std::tuple<double, double>> children_statistics;
        for (auto const& x : this->children){
            if (x.second == nullptr) std::cout << "nullptr" << std::endl;
            children_statistics.push_back(std::make_tuple(x.second->mean, x.second->standard_dev));
        }
        std::tuple<double, double> new_max_dist = compute_max_distribution(&children_statistics);
        this->mean = std::get<0>(new_max_dist);
        this->standard_dev = std::get<1>(new_max_dist);
    }else{
        // Update the distribution at the leaf
        this->alpha += new_reward;
        this->beta += weight - new_reward;

        // Update the distribution mean and std
        this->mean = alpha / (alpha + beta);
        this->standard_dev = sqrt(this->alpha * this->beta / ((this->alpha + this->beta) * (this->alpha + this->beta) * (this->alpha + this->beta + 1)));
    }

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
}

BayesianGaussianNode* BayesianGaussianNode::select_ucb_child(int virtual_loss, int weight) {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    if(this->is_terminal){
        this->open_no_visits += weight;
        if (this->access_lock) omp_unset_lock(&this->local_node_lock);
        return nullptr;
    }
    int optimal_action = -1;
    double optimal_action_ucb_value = -1.0;
    for(auto const &ent : this->children) {
        double this_ucb_value;
        if(!virtual_loss){
            if(this->no_visits != 0){
                this_ucb_value = ent.second->mean + sqrt(2 * log(this->no_visits)) * ent.second->standard_dev;
            }else{
                this_ucb_value = DBL_MAX;
            }
        }else{
            if(ent.second->open_no_visits + ent.second->no_visits == 0 || ent.second->no_visits == 0 || this->no_visits + this->open_no_visits == 0 || ent.second->open_no_visits + ent.second->no_visits - 1 == 0){
                this_ucb_value  = DBL_MAX;
            } else{
                this_ucb_value = (ent.second->no_visits/(ent.second->open_no_visits + ent.second->no_visits)) * ent.second->mean +
                                 sqrt(2 * log(this->no_visits + this->open_no_visits)) * sqrt(
                                 (ent.second->no_visits - 1) * ent.second->standard_dev * ent.second->standard_dev / (ent.second->open_no_visits + ent.second->no_visits - 1) +
                                 (ent.second->no_visits * ent.second->open_no_visits * ent.second->mean)/((ent.second->no_visits + ent.second->open_no_visits) * (ent.second->no_visits + ent.second->open_no_visits - 1)));
            }
            if(std::isnan(this_ucb_value)) this_ucb_value = DBL_MAX;
        }

        if(this_ucb_value > optimal_action_ucb_value){
            optimal_action = ent.first;
            optimal_action_ucb_value = this_ucb_value;
        }
    }
    if (optimal_action == -1) std::cout << "Returning -1!!!" << "The number of children was: " << this->children.size() << std::endl;
    this->open_no_visits += weight;
    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
    return this->children[optimal_action];
}

std::tuple<int, BayesianGaussianNode *> BayesianGaussianNode::select_optimal_child() {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    int optimal_action = -1;
    double max_visits = -1.0;
    for(auto &ent : this->children) {
        if(ent.second->mean > max_visits){
            optimal_action = ent.first;
            max_visits = ent.second->mean;
        }
    }

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);

    if (optimal_action == -1){
        return {-1, nullptr};
    }else{
        return {optimal_action, this->children[optimal_action]};
    }
}

void BayesianGaussianNode::initialize_children() {
    for(auto const &ent : state->children) {
        this->children[ent.first] = new BayesianGaussianNode(ent.second, this->access_lock);
    }
}


std::tuple<double, double> BayesianGaussianNode::get_statistics_for_global_sum(){
    return {this->local_proc_total_visits, this->local_proc_total_reward};
}


void BayesianGaussianNode::update_statistics_from_global_sum(double global_total_visits, double global_total_reward){
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

//    double old_alpha = this->alpha;
//    double old_beta = this->beta;

    this->alpha = 1 + global_total_reward;
    this->beta = 1 + global_total_visits - global_total_reward;

//    std::cout << "(" << old_alpha - this->alpha << ", " << old_beta - this->beta << ")" << std::endl;

    this->mean = alpha / (alpha + beta);
    this->standard_dev = sqrt(this->alpha * this->beta / ((this->alpha + this->beta) * (this->alpha + this->beta) * (this->alpha + this->beta + 1)));

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
}


void BayesianGaussianNode::get_terminals(std::list<BayesianGaussianNode*>* rtn){
    if (this->is_terminal){
        rtn->push_back(this);
    } else {
        for(auto const &ent : this->children) {
            ent.second->get_terminals(rtn);
        }
    }
}


void BayesianGaussianNode::get_shallow_nodes(int depth, std::list<BayesianGaussianNode*>* rtn){
    if (depth > 0){
        rtn->push_back(this);
        for(auto const &ent : this->children) {
            ent.second->get_shallow_nodes(depth - 1, rtn);
        }
    } else if (depth == 0) {
        rtn->push_back(this);
    }
}

void BayesianGaussianNode::global_backprop(){
    for(auto const &ent : this->children) {
        ent.second->global_backprop();
    }
    this->update_statistics(0, 0, 0);
}

std::tuple<double, double> compute_max_distribution(std::vector<std::tuple<double, double>> *children_statistics) {
    if(children_statistics->size() == 1){
        return (*children_statistics)[0];
    }else if (children_statistics->size() == 2){
        return compute_pairwise_max(std::get<0>((*children_statistics)[0]),
                                    std::get<0>((*children_statistics)[1]),
                                    std::get<1>((*children_statistics)[0]),
                                    std::get<1>((*children_statistics)[1]));
    }else{
        std::size_t const half_size = children_statistics->size() / 2;
        std::vector<std::tuple<double, double>> split_lo(children_statistics->begin(), children_statistics->begin() + half_size);
        std::vector<std::tuple<double, double>> split_hi(children_statistics->begin() + half_size, children_statistics->end());

        std::tuple<double, double> lo_stats = compute_max_distribution(&split_lo);
        std::tuple<double, double> hi_stats = compute_max_distribution(&split_hi);

        return compute_pairwise_max(std::get<0>(lo_stats),
                                    std::get<0>(hi_stats),
                                    std::get<1>(lo_stats),
                                    std::get<1>(hi_stats));
    }
}

std::tuple<double, double> compute_pairwise_max(double mu_1, double mu_2, double sigma_1, double sigma_2){
    double sigma_m = sigma_1 * sigma_1 + sigma_2 * sigma_2;
    double alpha = (mu_1 - mu_2) / sigma_m;
    double F1_alpha = F1(alpha);
    double F2_alpha = F2(alpha);
    double CDF_alpha = normal_cdf(alpha);

    double rtn_mean = mu_2 + sigma_m * F1_alpha;
    double rtn_std = sqrt(sigma_2 * sigma_2 + (sigma_1 * sigma_1 - sigma_2 * sigma_2) * CDF_alpha +
                          sigma_m * sigma_m * F2_alpha);

    return std::make_tuple(rtn_mean, rtn_std);
}

// F_1 and F_2 as defined in https://arxiv.org/pdf/1203.3519.pdf
double F1(double alpha){
    return alpha * normal_cdf(alpha) + normal_pdf(alpha);
}

double F2(double alpha){
    double cdf_alpha = normal_cdf(alpha);
    double pdf_alpha = normal_pdf(alpha);
    return alpha * alpha * cdf_alpha * (1 - cdf_alpha) +
           (1 - 2 * cdf_alpha) * alpha * pdf_alpha -
           pdf_alpha * pdf_alpha;
}

// Functions defining the standard normal distribution

double normal_cdf(double alpha){
    return 0.5 * erfc(-alpha * M_SQRT1_2);
}

double normal_pdf(double alpha){
    return (1/sqrt(2.0 * M_PI)) * std::exp(-0.5 * alpha * alpha);

}
