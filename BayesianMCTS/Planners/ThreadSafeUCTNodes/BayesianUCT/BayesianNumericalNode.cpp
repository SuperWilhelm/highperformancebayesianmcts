#include "BayesianNumericalNode.h"
#include <iostream>
#include <cfloat>
#include <boost/math/distributions/beta.hpp>
#include <cmath>
#include <list>

BayesianNumericalNode::BayesianNumericalNode(ProblemState *game_state, bool access_lock) : UCTNode(game_state,
                                                                                                   access_lock) {
    // Start with a uniform distribution
    this->distribution = new double [1000];
    for (int i = 0; i < 1000; ++i){
         this->distribution[i] = 1.0;
    }
    this->no_visits = 2;
    this->initialize_children();
}

void BayesianNumericalNode::update_statistics(double new_reward, int weight, int maintain_local_statistics) {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    if(maintain_local_statistics){
        this->local_proc_total_visits += weight;
        this->local_proc_total_reward += new_reward;
    }

    // Keep track of the visits count
    this->no_visits += weight;
    this->open_no_visits -= weight;

    if(!this->is_terminal){
        // Take extremum distribution
        for (int i = 0; i < 1000; ++i){
            this->distribution[i] = 1.0;
        }

        for (int child_index = 0; child_index < this->children.size(); child_index++){
            for(int i = 0; i < 1000; ++i){
                this->distribution[i] *= (this->children[child_index]->distribution[i]);
            }
        }
        // The new internal distribution has been calculated.
        // https://stats.stackexchange.com/questions/10159/find-expected-value-using-cdf
        // Recalculate the mean.
        this->mean = 0;
        for (int i = 0; i < 1000; ++i){
            this->mean += (1.0 - this->distribution[i])/1000.0;
        }
        // Now calculate the second moment
        double second_moment = 0.0;
        for (int i = 0; i < 1000; ++i){
            second_moment += ((i * 0.001 + 0.0005) * (1.0 - this->distribution[i])) / 1000.0;
        }
        second_moment *= 2.0;
        // Use this to calculate the std
        this->standard_dev = sqrt(second_moment - this->mean * this->mean);
    }else{
	// Update the distribution at the leaf
        this->alpha += new_reward;
        this->beta += weight - new_reward;

        // Update the distribution mean and std
        this->mean = this->alpha / (this->alpha + this->beta);
        this->standard_dev = sqrt(this->alpha * this->beta / ((this->alpha + this->beta) * (this->alpha + this->beta) * (this->alpha + this->beta + 1)));

        boost::math::beta_distribution<> mybeta(this->alpha, this->beta);

        for (int i = 0; i < 1000; ++i){
            this->distribution[i] = boost::math::cdf(mybeta, i * 0.001 + 0.0005);
        }
    }

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
}

BayesianNumericalNode* BayesianNumericalNode::select_ucb_child(int virtual_loss, int weight) {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    if (this->is_terminal){
        this->open_no_visits += weight;
        if (this->access_lock) omp_unset_lock(&this->local_node_lock);
        return nullptr;
    }

    int optimal_action = -1;
    double optimal_action_ucb_value = -1.0;
    for(auto const &ent : this->children) {
        double this_ucb_value;
        if(!virtual_loss){
            if(this->no_visits != 0){
                this_ucb_value = ent.second->mean + sqrt(2 * log(this->no_visits)) * ent.second->standard_dev;
            }else{
                this_ucb_value = DBL_MAX;
            }
        }else{
            if(ent.second->open_no_visits + ent.second->no_visits == 0 || ent.second->no_visits == 0 || this->no_visits + this->open_no_visits == 0 || ent.second->open_no_visits + ent.second->no_visits - 1 == 0){
                this_ucb_value  = DBL_MAX;
            } else{
                this_ucb_value = (ent.second->no_visits/(ent.second->open_no_visits + ent.second->no_visits)) * ent.second->mean +
                                 sqrt(2 * log(this->no_visits + this->open_no_visits)) * sqrt(
                                 (ent.second->no_visits - 1) * ent.second->standard_dev * ent.second->standard_dev / (ent.second->open_no_visits + ent.second->no_visits - 1) +
                                 (ent.second->no_visits * ent.second->open_no_visits * ent.second->mean)/((ent.second->no_visits + ent.second->open_no_visits) * (ent.second->no_visits + ent.second->open_no_visits - 1)));
            }
            if(this_ucb_value < -1) this_ucb_value = DBL_MAX;
        }

        if(this_ucb_value > optimal_action_ucb_value){
            optimal_action = ent.first;
            optimal_action_ucb_value = this_ucb_value;
        }
    }

    this->open_no_visits += weight;
    if (this->access_lock) omp_unset_lock(&this->local_node_lock);

    return this->children[optimal_action];
}

std::tuple<int, BayesianNumericalNode *> BayesianNumericalNode::select_optimal_child() {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    int optimal_action = -1;
    double max_visits = -1.0;

    for(auto &ent : this->children) {
        if(ent.second->mean > max_visits){
            optimal_action = ent.first;
            max_visits = ent.second->mean;
        }
    }

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);

    if (optimal_action == -1){
        return {-1, nullptr};
    }else{
        return {optimal_action, this->children[optimal_action]};
    }
}

void BayesianNumericalNode::initialize_children() {
    for(auto const &ent : state->children) {
        this->children[ent.first] = new BayesianNumericalNode(ent.second, this->access_lock);
    }
}



void BayesianNumericalNode::get_terminals(std::list<BayesianNumericalNode*>* rtn){
    if (this->is_terminal){
        rtn->push_back(this);
    } else {
        for(auto const &ent : this->children) {
            ent.second->get_terminals(rtn);
        }
    }
}


std::tuple<double, double> BayesianNumericalNode::get_statistics_for_global_sum(){
    return {this->local_proc_total_visits, this->local_proc_total_reward};
}


void BayesianNumericalNode::update_statistics_from_global_sum(double global_total_visits, double global_total_reward){
    if (!this->is_terminal) std::cout << "Error: Update only works on leaf nodes" << std::endl;

    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    this->alpha = 1 + global_total_reward;
    this->beta = 1 + global_total_visits - global_total_reward;

    // Update the distribution mean and std
    this->mean = this->alpha / (this->alpha + this->beta);
    this->standard_dev = sqrt(this->alpha * this->beta / ((this->alpha + this->beta) * (this->alpha + this->beta) * (this->alpha + this->beta + 1)));

    boost::math::beta_distribution<> mybeta(this->alpha, this->beta);

    for (int i = 0; i < 1000; ++i){
        this->distribution[i] = boost::math::cdf(mybeta, i * 0.001 + 0.0005);
    }

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
}


void BayesianNumericalNode::get_shallow_nodes(int depth, std::list<BayesianNumericalNode*>* rtn){
    if (depth > 0){
        rtn->push_back(this);
        for(auto const &ent : this->children) {
            ent.second->get_shallow_nodes(depth - 1, rtn);
        }
    } else if (depth == 0) {
        rtn->push_back(this);
    }
}


void BayesianNumericalNode::global_backprop(){
    for(auto const &ent : this->children) {
        ent.second->global_backprop();
    }
    this->update_statistics(0, 0, 0);
}
