#ifndef BAYESIANMCTS_BAYESIANGAUSSIANNODE_H
#define BAYESIANMCTS_BAYESIANGAUSSIANNODE_H

#include "../UCTNode.h"
#include <list>
class BayesianGaussianNode: public UCTNode {
public:
    BayesianGaussianNode(ProblemState* game_state,  bool access_lock);
    BayesianGaussianNode() = default;
    double mean = 0.5;
    double standard_dev = 0.28867513459;

    double alpha = 1.0;
    double beta = 1.0;

    std::map<int, BayesianGaussianNode*> children = {};

    void update_statistics(double new_reward, int weight, int store_local_proc_results);
    BayesianGaussianNode* select_ucb_child(int virtual_loss, int weight);
    std::tuple<int, BayesianGaussianNode*> select_optimal_child();
    void initialize_children();

    std::tuple<double, double> get_statistics_for_global_sum();
    void update_statistics_from_global_sum(double global_total_visits, double global_total_reward);

    void get_shallow_nodes(int depth, std::list<BayesianGaussianNode*>* rtn);
    void get_terminals(std::list<BayesianGaussianNode*>* rtn);

    int share_type = -1;
    int force_global_bprop = 1;

    void global_backprop();
};

#endif
