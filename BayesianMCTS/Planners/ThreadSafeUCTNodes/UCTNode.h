#ifndef BAYESIANMCTS_UCTNODE_H
#define BAYESIANMCTS_UCTNODE_H
#include <map>
#include <omp.h>
#include "../../Problems/ProblemStates/ProblemState.h"


class UCTNode {
protected:
    // Locking settings and the locks
    bool access_lock;
    omp_lock_t local_node_lock{};

    // Functions to obtain and release locks at the requested granularity
public:
    ProblemState* state;
    explicit UCTNode(ProblemState* game_sate, bool access_lock);
    UCTNode() = default;
    // Stats to store for all nodes.
    int is_terminal;
    double open_no_visits = 0;
    double no_visits = 0;

    // Required for root parallelizm
    double local_proc_total_visits = 0;
    double local_proc_total_reward = 0;
};

#endif
