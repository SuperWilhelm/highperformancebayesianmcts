#ifndef BAYESIANMCTS_FREQUENTESTNODE_H
#define BAYESIANMCTS_FREQUENTESTNODE_H

#include <map>
#include "../../../Problems/ProblemStates/ProblemState.h"
#include "../UCTNode.h"
#include <list>

class FrequentestNode: public UCTNode {
    double average_reward = -1.0;
    std::map<int, ProblemState*> valid_actions_for_expansion = {};
public:
    std::map<int, FrequentestNode*> children = {};
    FrequentestNode(ProblemState* game_state,  bool access_lock);
    FrequentestNode() = default;
    void update_statistics(double new_reward, int weight, int store_local_proc_results);
    FrequentestNode* select_ucb_child(int virtual_loss, int weight);
    std::tuple<int, FrequentestNode*> select_optimal_child();

    std::tuple<double, double> get_statistics_for_global_sum();
    void update_statistics_from_global_sum(double global_total_visits, double global_total_reward);

    void get_shallow_nodes(int depth, std::list<FrequentestNode*>* rtn);
    void get_terminals(std::list<FrequentestNode*>* rtn);

    int share_type = 4;
    int force_global_bprop = 0;

    void global_backprop();
};

#endif
