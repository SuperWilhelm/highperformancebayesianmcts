#include <iostream>
#include "FrequentestNode.h"
#include <math.h>
#include <float.h>

FrequentestNode::FrequentestNode(ProblemState *game_state,  bool access_lock): UCTNode(game_state, access_lock) {
    this->is_terminal = game_state->terminal();

    if(!this->is_terminal){
        for(auto &ent : game_state->children) {
            this->valid_actions_for_expansion[ent.first] = ent.second;
        }
    }
}

void FrequentestNode::update_statistics(double new_reward, int weight, int maintain_local_statistics) {

    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    if(maintain_local_statistics){
        this->local_proc_total_visits += weight;
        this->local_proc_total_reward += new_reward;
    }

    if(no_visits == 0){
        average_reward = new_reward;
    }else{
        average_reward = (no_visits / (no_visits + weight)) * average_reward +
                         (weight / (no_visits + weight)) * new_reward;
    }

    this->no_visits += weight;
    this->open_no_visits -= weight;

    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
}

FrequentestNode* FrequentestNode::select_ucb_child(int virtual_loss, int weight) {

    if (this->access_lock) omp_set_lock(&this->local_node_lock);


    if(this->is_terminal || this->no_visits == 0){
        this->open_no_visits += weight;

        if(this->access_lock) omp_unset_lock(&this->local_node_lock);

        return nullptr;
    }

    if(!this->valid_actions_for_expansion.empty()){
        // If there is a unexpanded child, add it to the tree and return the new value
        auto it = this->valid_actions_for_expansion.begin();
        std::advance(it, rand() % this->valid_actions_for_expansion.size());
        int random_key = it->first;
        this->children[random_key] = new FrequentestNode((this->valid_actions_for_expansion)[random_key],
                                                         this->access_lock);
        this->valid_actions_for_expansion.erase(random_key);

        this->open_no_visits += weight;

        if (this->access_lock) omp_unset_lock(&this->local_node_lock);

        return this->children[random_key];
    }else{
        // There are no unexpanded children - Select the next state using the UCB1 formula
        int optimal_action = -1;
        double optimal_action_ucb_value = -1.0;
        for(auto const &ent : this->children) {
            double this_ucb_value;
            if(!virtual_loss){
                // Appeal to the basic formula
                if(ent.second->no_visits != 0){
                    this_ucb_value = dynamic_cast<FrequentestNode*>(ent.second)->average_reward +
                                     M_SQRT2 * sqrt(2*log(this->no_visits) / ent.second->no_visits);
                }else{
                    this_ucb_value = DBL_MAX;
                }
            }else{
                if(ent.second->no_visits + ent.second->open_no_visits != 0){ // To solve race condition of flag yet to be updated below
                    this_ucb_value = (ent.second->no_visits / (ent.second->no_visits + ent.second->open_no_visits)) * dynamic_cast<FrequentestNode*>(ent.second)->average_reward +
                                     M_SQRT2 * sqrt(2*log(this->no_visits + this->open_no_visits) / (ent.second->no_visits+ent.second->open_no_visits));
                }else{
                    this_ucb_value = DBL_MAX;
                }
            }

            if(this_ucb_value > optimal_action_ucb_value){
                optimal_action = ent.first;
                optimal_action_ucb_value = this_ucb_value;
            }
        }

        this->open_no_visits += weight;

        if(this->access_lock) omp_unset_lock(&this->local_node_lock);
        return this->children[optimal_action];
    }
}

std::tuple<int, FrequentestNode*> FrequentestNode::select_optimal_child() {
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    int optimal_action = -1;
    double max_visits = -1.0;

    for(auto &ent : this->children) {
        if(ent.second->no_visits > max_visits){
            optimal_action = ent.first;
            max_visits = ent.second->no_visits;
        }
    }
    if (this->access_lock) omp_unset_lock(&this->local_node_lock);

    if (optimal_action == -1){
        // Case: No data yet on children
        return {-1, nullptr};
    }else{
        return {optimal_action, this->children[optimal_action]};
    }
}



std::tuple<double, double> FrequentestNode::get_statistics_for_global_sum(){
    return {this->local_proc_total_visits, this->local_proc_total_reward};
}


void FrequentestNode::update_statistics_from_global_sum(double global_total_visits, double global_total_reward){
    if (this->access_lock) omp_set_lock(&this->local_node_lock);

    this->no_visits = global_total_visits;
    this->average_reward = global_total_reward / global_total_visits;
    if (this->access_lock) omp_unset_lock(&this->local_node_lock);
}


void FrequentestNode::get_terminals(std::list<FrequentestNode*>* rtn){
    if (this->is_terminal){
        rtn->push_back(this);
    } else {
        for(auto const &ent : this->children) {
            ent.second->get_terminals(rtn);
        }
    }
}


void FrequentestNode::get_shallow_nodes(int depth, std::list<FrequentestNode*>* rtn){
    if (depth > 0){
        rtn->push_back(this);
        for(auto const &ent : this->children) {
            ent.second->get_shallow_nodes(depth - 1, rtn);
        }
    } else if (depth == 0) {
        rtn->push_back(this);
    }
}


void FrequentestNode::global_backprop(){
    for(auto const &ent : this->children) {
        ent.second->global_backprop();
    }
    this->update_statistics(0, 0, 0);
}
