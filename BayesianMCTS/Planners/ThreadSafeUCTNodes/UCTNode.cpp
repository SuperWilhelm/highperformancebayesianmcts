#include "UCTNode.h"
//#include <omp.h>
#include <iostream>

UCTNode::UCTNode(ProblemState* game_state, bool access_lock) {
    this->state = game_state;
    this->access_lock = access_lock;
    this->is_terminal = game_state->terminal();
    omp_init_lock(&this->local_node_lock);
}
