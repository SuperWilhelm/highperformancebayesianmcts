#ifndef BAYESIANMCTS_PARALLELUCTPLANNER_H
#define BAYESIANMCTS_PARALLELUCTPLANNER_H
#include "../Problems/ProblemStates/ProblemState.h"
#include "../Problems/Problem.h"
#include <mpi.h>
#include <omp.h>
#include <cmath>
#include <iostream>
#include "ParallelUCTPlanner.h"
#include <list>
#include "../Problems/ProblemStates/ProblemStateLeafBernoulli.h"

#include <chrono>

#define NO_MUTEX 0
#define LOCAL_MUTEX 1
#define GLOBAL_MUTEX 2

#define LEAF 1
#define TREE_LOCAL_VL 2
#define TREE_LOCAL_NO_VL 3
#define TREE_GLOBAL_VL 4
#define TREE_GLOBAL_NO_VL 5
#define ROOT_WITHOUT_WEIGHT_SHARING 6
#define ROOT_WITH_WEIGHT_SHARING 7
#define UCT_TREESPLIT 8

#define UCT 0
#define BAYESIAN_UCT_GAUSSIAN 1
#define BAYESIAN_UCT_NUMERICAL 2

#define ROOT_PARALLEL_NODE_SHARING_DEPTH 3

template<class T>
class ParallelUCTPlanner{
    T root;

    // Parallel Parameters
    int parallel_mode{};
    int no_threads{};
    int virtual_loss{};
    int access_type{};

    // MPI Parameters
    int rank{};
    int size{};

    omp_lock_t global_tree_lock{};

    int inference_mode{};

    int root_share_count = -1;
    int root_min_leaf_id = -1;

    std::vector<std::mt19937> rng_s;
public:
    explicit ParallelUCTPlanner(Problem *problem, int inference_mode, int parallel_mode, int rank, int size);

    double plan(int no_iterations, int logging_rate);
    std::vector<T*> select(T *node);
    double simulate(T *from_node);
    void backprop(std::vector<T*> nodes, double total_reward, int simulations_count);
    void rollout(T *from_node);

    double evaluate();

    double root_parallelization_evaluate();
    void root_parallelization_share_data();
};

template <class T>
ParallelUCTPlanner<T>::ParallelUCTPlanner(Problem *problem, int inference_mode, int parallel_mode, int rank, int size){
    this->rank = rank;
    this->size = size;

    srand(this->rank + 4);  // srand(0) and srand(1) generate the same sequence
    problem->re_seed(this->rank + 4);
    // Initialize parallel parameters
    this->parallel_mode = parallel_mode;
    this->inference_mode = inference_mode;


    // Decided if virtual loss is required:
    if (this->parallel_mode == TREE_LOCAL_VL || this->parallel_mode == TREE_GLOBAL_VL){
        this->virtual_loss = 1;
    }else{
        this->virtual_loss = 0;
    }

    // Decide what serialization is required inside nodes
    if(this->parallel_mode == TREE_LOCAL_VL || this->parallel_mode == TREE_LOCAL_NO_VL){
        this->access_type = LOCAL_MUTEX;
    }else if(this->parallel_mode == TREE_GLOBAL_VL || this->parallel_mode == TREE_GLOBAL_NO_VL){
        this->access_type = GLOBAL_MUTEX;
    }else{
        this->access_type = NO_MUTEX;
    }

    // Record the number of thread which will run the planner
    this->no_threads = omp_get_max_threads();
    if(this->parallel_mode != TREE_LOCAL_NO_VL && 
       this->parallel_mode != TREE_LOCAL_VL &&       
       this->parallel_mode != TREE_GLOBAL_NO_VL &&       
       this->parallel_mode != TREE_GLOBAL_VL &&       
       this->parallel_mode != LEAF){
       this->no_threads = 1;
    }
    // For each thread create a random number generator
    for (int i = 0; i < this->no_threads;i++){
        // Seed each rng with unique integer
        this->rng_s.push_back(std::mt19937(i + this->no_threads*(this->rank + 4))); // Seed based on thread index and rank
    }
    this->root = T(problem->get_start_state(), this->access_type == LOCAL_MUTEX);

    if(this->access_type == GLOBAL_MUTEX) omp_init_lock(&this->global_tree_lock);
}


template <class T>
double ParallelUCTPlanner<T>::plan(int no_iterations, int logging_rate) {
    logging_rate = 25;
    bool terminate = 0;
    double start_time = omp_get_wtime();
    if(this->parallel_mode == TREE_LOCAL_VL || this->parallel_mode == TREE_LOCAL_NO_VL||
       this->parallel_mode == TREE_GLOBAL_VL || this->parallel_mode == TREE_GLOBAL_NO_VL) {
        // Perform iterations in parallel
        int completed_its = 0;
#pragma omp parallel for
        for (int i = 1; i <= no_iterations; i++) {
            rollout(&this->root);
#pragma omp cancel for if(terminate)
#pragma omp critical
{
            completed_its += 1;
            if (completed_its % logging_rate == 0){
                if(this->evaluate() / (this->root).state->minimax_value > 0.99){
                    std::cout << "(" << completed_its << ", " << omp_get_wtime() - start_time << ")" << std::endl;
                    terminate = 1;
                }
            }
}
        }
    }else{
        // Standard rollout method here
        for (int i = 1; i <= no_iterations; i++) {
            rollout(&this->root);
            if (i % logging_rate == 0){
                double value = this->evaluate()/ (this->root).state->minimax_value;
                if (value > 0.99){
                    if (this->rank == 0) std::cout << "(" << i << ", " << omp_get_wtime() - start_time << ")" << std::endl; 
                    return -1.00;
                }
            }
        }
    }
    return this->evaluate();
}

template <class T>
void ParallelUCTPlanner<T>::rollout(T* from_node) {
    std::vector<T*> node_sequence = select(from_node);
    if (this->parallel_mode == LEAF){
        // This is for the case of leaf parallelization
        double reward_total = 0;
#pragma omp parallel for reduction(+:reward_total)
        for (int i = 0; i < this->no_threads; i++){
            reward_total += simulate(node_sequence[node_sequence.size() - 1]);
        }
//        backprop(node_sequence, reward_total, this->no_threads);
        reward_total = reward_total/this->no_threads;
        backprop(node_sequence, reward_total, 1);
    }else{
        // In all other cases perform standard rollout
        double utility = simulate(node_sequence[node_sequence.size() - 1]);
        backprop(node_sequence, utility, 1);
    }
}

template <class T>
std::vector<T*> ParallelUCTPlanner<T>::select(T* node){
    if(this->access_type == GLOBAL_MUTEX) omp_set_lock(&this->global_tree_lock);

    std::vector<T*> rtn;
    T* current_node = node;
    rtn.push_back(current_node);

    int select_weight;
    if(this->parallel_mode == LEAF){
        select_weight = this->no_threads;
    }else{
        select_weight = 1;
    }
    // TODO: REMOVE
    select_weight = 1;

    while (current_node != nullptr){
        current_node = current_node->select_ucb_child(this->virtual_loss, select_weight);
        if(current_node != nullptr){
            rtn.push_back(current_node);
        }
    }

    if(this->access_type == GLOBAL_MUTEX) omp_unset_lock(&this->global_tree_lock);

    return rtn;
}

template <class T>
double ParallelUCTPlanner<T>::simulate(T* from_node){

    ProblemState* current_problem_state = from_node->state;

    while (!current_problem_state->terminal() && !current_problem_state->children.empty()) {
        auto it = current_problem_state->children.begin();
        std::advance(it, rand() % current_problem_state->children.size());
        int random_key = it->first;
        current_problem_state = current_problem_state->children[random_key];
    }
    return 
dynamic_cast<ProblemStateLeafBernoulli*>(current_problem_state)->sampleReward(&this->rng_s[omp_get_thread_num()]);
}

template <class T>
void ParallelUCTPlanner<T>::backprop(std::vector<T*> nodes, double value, int weight) {
    if(this->access_type == GLOBAL_MUTEX) omp_set_lock(&this->global_tree_lock);

    for (auto it = nodes.rbegin(); it != nodes.rend(); ++it){
        (*it)->update_statistics(value, weight, this->parallel_mode == ROOT_WITH_WEIGHT_SHARING ||
                                                this->parallel_mode == ROOT_WITHOUT_WEIGHT_SHARING);
    }

    if(this->access_type == GLOBAL_MUTEX) omp_unset_lock(&this->global_tree_lock);
}

template <class T>
double ParallelUCTPlanner<T>::evaluate() {

    if(this->access_type == GLOBAL_MUTEX) omp_set_lock(&this->global_tree_lock);

    if(this->parallel_mode == ROOT_WITHOUT_WEIGHT_SHARING ||
       this->parallel_mode == ROOT_WITH_WEIGHT_SHARING){
        double value = root_parallelization_evaluate();
        root_parallelization_share_data();
        if(this->access_type == GLOBAL_MUTEX) omp_unset_lock(&this->global_tree_lock);
        return value;
    }else{
        std::tuple<int, T*> step;
        T* current_node = &this->root;
        while(!current_node->is_terminal){
            step = current_node->select_optimal_child();
            if (std::get<0>(step) != -1){
                current_node = std::get<1>(step);
            }else{
                break;
            }
        }
        if(this->access_type == GLOBAL_MUTEX) omp_unset_lock(&this->global_tree_lock);
        return current_node->state->average_value;
    }
}

template <class T>
double ParallelUCTPlanner<T>::root_parallelization_evaluate(){
    // First select the best option through majority vote
    bool global_result_established = false;
    bool this_proc_still_voting = true;
    T current_node = this->root;
    ProblemState *current_problem_state = current_node.state;
    while(!global_result_established){
        int *our_vote = new int[current_problem_state->children.size()];
        for(auto const &ent : current_problem_state->children) {
            our_vote[ent.first] = 0;
        }
        int *totals = new int[current_problem_state->children.size()];

        if(this_proc_still_voting){
            for(auto const &ent : current_node.children) {
                our_vote[ent.first] = ent.second->no_visits;
            }
        }

        // Perform global sum
        MPI_Allreduce(our_vote,
                      totals,
                      current_problem_state->children.size(),
                      MPI_INT,
                      MPI_SUM,
                      MPI_COMM_WORLD);

        int max_visits_seen = 0;
        int max_action = -1;
        for(int i = 0; i < current_problem_state->children.size();i++){
            if(totals[i] > max_visits_seen){
                max_action = i;
                max_visits_seen = totals[i];
            }
        }
        if(max_action == -1){
            global_result_established = true;
        }else{
            // Update the pointer to the current problem state
            current_problem_state = current_problem_state->children[max_action];
            if(this_proc_still_voting &&
                current_node.children.find(max_action) != current_node.children.end()){
                current_node = *current_node.children[max_action];
            }else{
                this_proc_still_voting = false;
            }
        }
    }

    // Finally return the value of the voted action sequence
    return current_problem_state->average_value;
}


template <class T>
void ParallelUCTPlanner<T>::root_parallelization_share_data(){
    // nodes_for_sharing = {0: No sharing, -1: Leaves only, n: Top n layers of the tree} 

    int nodes_for_sharing;

    if (this->parallel_mode == ROOT_WITHOUT_WEIGHT_SHARING){
        nodes_for_sharing = 0;
    } else if (this->parallel_mode == ROOT_WITH_WEIGHT_SHARING){
        if(this->inference_mode == BAYESIAN_UCT_GAUSSIAN ||
           this->inference_mode == BAYESIAN_UCT_NUMERICAL){
            nodes_for_sharing = -1;    
        }else if(this->inference_mode == UCT){
            nodes_for_sharing = ROOT_PARALLEL_NODE_SHARING_DEPTH;
        }
    }

    if (nodes_for_sharing != 0){
        // There is data to be shared
        if (root_share_count == -1){
             if(nodes_for_sharing == -1){
                 std::tuple<double, double> share_parameters = (this->root).state->get_terminal_data();
                 root_share_count = std::get<0>(share_parameters) * 2;
                 root_min_leaf_id = std::get<1>(share_parameters);
             }else{
                 root_share_count = (this->root).state->get_child_count_to_depth(nodes_for_sharing) * 2; // Get node count to depth
             }
        }
        std::list<T*> our_shared_nodes;

        if(nodes_for_sharing == -1){
            // Find all the terminals currently in our tree
            (this->root).get_terminals(&our_shared_nodes);
        }else{
            // Find all of the leaves in our tree in the first nodes_for_sharing layers
            (this->root).get_shallow_nodes(nodes_for_sharing, &our_shared_nodes);
        }

        // Our nodes for sharing are in nodes_for_sharing
        // Input their statistics into our vote

        double *our_vote = new double[root_share_count];

        for (int i = 0; i < root_share_count; i++) our_vote[i] = 0;

        for(const auto& node : our_shared_nodes){
            std::tuple<double, double> node_vote = node->get_statistics_for_global_sum();
            if(nodes_for_sharing == -1){
                our_vote[2 * (node->state->state_id - root_min_leaf_id)] = std::get<0>(node_vote);
                our_vote[2 * (node->state->state_id - root_min_leaf_id) + 1] = std::get<1>(node_vote);
            }else{
                our_vote[2 * node->state->state_id] = std::get<0>(node_vote);
                our_vote[2 * node->state->state_id + 1] = std::get<1>(node_vote);
            }
        }

        double *totals = new double[root_share_count];

        // Send the data
        MPI_Allreduce(our_vote,
                      totals,
                      root_share_count,
                      MPI_DOUBLE,
                      MPI_SUM,
                      MPI_COMM_WORLD);

        // Share the new totals our data structure
        for(const auto& node : our_shared_nodes){
            if(nodes_for_sharing == -1){
                node->update_statistics_from_global_sum(totals[2 * (node->state->state_id - root_min_leaf_id)],
                                                        totals[2 * (node->state->state_id - root_min_leaf_id) + 1]);
            }else{
                node->update_statistics_from_global_sum(totals[2 * node->state->state_id],
                                                        totals[2 * node->state->state_id + 1]);
            }
        }

        
        if(this->inference_mode == BAYESIAN_UCT_GAUSSIAN ||
           this->inference_mode == BAYESIAN_UCT_NUMERICAL){
            (this->root).global_backprop();
        }
    }
}

#endif
