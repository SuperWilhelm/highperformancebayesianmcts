#include <iostream>
#include "Planners/ParallelUCTPlanner.h"
#include "Planners/UCTTreesplit.h"
#include "Problems/Problem.h"
#include "Planners/ThreadSafeUCTNodes/FrequentestUCT/FrequentestNode.h"
#include "Planners/ThreadSafeUCTNodes/BayesianUCT/BayesianGaussianNode.h"
#include "Planners/ThreadSafeUCTNodes/BayesianUCT/BayesianNumericalNode.h"
#include <mpi.h>
#include <omp.h>

#define SERIAL 0
#define LEAF 1
#define TREE_LOCAL_VL 2
#define TREE_LOCAL_NO_VL 3
#define TREE_GLOBAL_VL 4
#define TREE_GLOBAL_NO_VL 5
#define ROOT_WITHOUT_WEIGHT_SHARING 6
#define ROOT_WITH_WEIGHT_SHARING 7
#define UCT_TREESPLIT 8

int main(int argc, char** argv) {
    /*
        The command line arguments are:
           1: planner: 0: "UCT"
                       1: "BayesianUCT-Gaussian"
                       2: "BayesianUCT-Numerical"
           2: planner parallelization: 0: "Serial",
                                       1: "Leaf",
                                       2: "Tree-Local-VL",
                                       3: "Tree-Local-no_VL"
                                       4: "Tree-Global-VL"
                                       5: "Tree-Global-no_VL"
                                       6: "Root_without_weight_sharing"
                                       7: "Root_with_weight_sharing"
                                       8: "UCT_Treesplit"
           3: planner max iterations: The maximum number of iterations of the solver
           4: random problem size: Integer - The size of the test problem
           5: random problem min branching factor: Integer - The minimum number of actions leading out of a node
           6: random problem max branching factor: Integer - The maximum number of actions leading out of a node
           7: random problem generation seed:      Integer - The seed of the random number generator

     */
    // Quit if params provided are incorrect
    if (argc != 8){
        std::cout << "Please enter the correct number of parameters" << std::endl;
        return 0;
    }

    //Parse the parameters
    int inference_mode = std::stoi(argv[1]);
    int parallel_mode = std::stoi(argv[2]);
    int no_iterations = std::stoi(argv[3]);
    int problem_size = std::stoi(argv[4]);
    int min_branching_factor = std::stoi(argv[5]);
    int max_branching_factor = std::stoi(argv[6]);
    int random_seed = std::stoi(argv[7]);
    int no_threads = omp_get_max_threads();

    int is_main_proc = 0;
    int size = 0;
    int rank = 0;

    if(parallel_mode == ROOT_WITHOUT_WEIGHT_SHARING ||
       parallel_mode == ROOT_WITH_WEIGHT_SHARING ||
       parallel_mode == UCT_TREESPLIT){
        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        if(rank == 0){
            is_main_proc = 1;
        }
    }else{
        is_main_proc = 1;
    }

    if(is_main_proc) {
        std::cout << "\"" << random_seed + 100 * parallel_mode + 10000 * no_threads + 10000000 * inference_mode <<  "\":{\"threads\": " 
<< no_threads << "," << std::endl;
        std::cout << "\"parallel_pattern\":" << parallel_mode << "," << std::endl;
        std::cout << "\"inference_mode\":" << inference_mode << "," << std::endl;
        std::cout << "\"random_seed\": " << random_seed << "," << std::endl;
        std::cout << "\"min_branching_factor\":" << min_branching_factor << "," << std::endl;
        std::cout << "\"max_branching_factor\":" << max_branching_factor << "," << std::endl;
        std::cout << "\"problem_size\":" << problem_size << "," << std::endl;
        std::cout << "\"size\":" << size << "," << std::endl;
        std::cout << "\"data\": {" << std::endl;
    }


    if(parallel_mode != UCT_TREESPLIT){
        Problem* problem = Problem::generate_random(random_seed, min_branching_factor, max_branching_factor, problem_size);

        if(inference_mode == 0){
            // Frequentest Inference
            ParallelUCTPlanner<FrequentestNode> planner = ParallelUCTPlanner<FrequentestNode>(problem, inference_mode, parallel_mode, rank, size);
            planner.plan(no_iterations, 100);
        }else if(inference_mode == 1){
            // Bayesian Gaussian Inference
            ParallelUCTPlanner<BayesianGaussianNode> planner = ParallelUCTPlanner<BayesianGaussianNode>(problem, inference_mode, parallel_mode, rank, size);
            planner.plan(no_iterations, 100);
        }else if(inference_mode == 2){
            ParallelUCTPlanner<BayesianNumericalNode> planner = ParallelUCTPlanner<BayesianNumericalNode>(problem, inference_mode, parallel_mode, rank, size);
            planner.plan(no_iterations, 100);
        }else{
            std::cout << "Error: The inference mode is not recognized" << std::endl;
        }

        if(is_main_proc) std::cout << "}}," << std::endl;
    }else{
        std::cout << "Coming Soon: UCT-Treesplit\n" << std::endl;
    }

    if(parallel_mode == ROOT_WITHOUT_WEIGHT_SHARING ||
       parallel_mode == ROOT_WITH_WEIGHT_SHARING ||
       parallel_mode == UCT_TREESPLIT){
        MPI_Finalize();
    }

    return 0;
}
